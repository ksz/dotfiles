# Available variants:

- [dwm](https://dwm.suckless.org/)
- [labwc](https://labwc.github.io/)
- [LXQt](https://lxqt-project.org/)
- [MATE](https://mate-desktop.org/)
- [Openbox](https://openbox.org/)
- [Termux](https://termux.dev/en/)
- [Xfce](https://xfce.org/)
