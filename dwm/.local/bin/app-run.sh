#!/bin/sh

function_freac () {
    if test -f /opt/appimages/freac.AppImage; then
        /opt/appimages/freac.AppImage --scale:1.10
    else
        dunstify -t 5000 "fre:ac is not installed."
    fi
}

function_gparted () {
    if which gparted; then
        sudo -A gparted
    else
        dunstify -t 5000 "GParted is not installed."
    fi
}

function_koreader () {
    if test -f /opt/appimages/koreader.AppImage; then
        /opt/appimages/koreader.AppImage
    else
        dunstify -t 5000 "KOReader is not installed."
    fi
}

function_ventoy () {
    if which ventoygui; then
        sudo -A ventoygui
    else
        dunstify -t 5000 "Ventoy is not installed."
    fi
}

case $1 in
    -f) function_freac ;;
    -g) function_gparted ;;
    -k) function_koreader ;;
    -v) function_ventoy ;;
esac
