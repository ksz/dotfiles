#!/bin/sh

function_hdmi () {
    pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo
    pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo
    pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo hdmi-output-0
}

function_dp1 () {
    pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo-extra1
    pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1
    pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1 hdmi-output-1
}

function_dp2 () {
    pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo-extra2
    pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra2
    pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra2 hdmi-output-2
}

function_lineout () {
    pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:analog-stereo
    pactl set-default-sink alsa_output.pci-0000_00_1f.3.analog-stereo
    pactl set-sink-port alsa_output.pci-0000_00_1f.3.analog-stereo analog-output-lineout
}

function_speaker () {
    pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:analog-stereo
    pactl set-default-sink alsa_output.pci-0000_00_1f.3.analog-stereo
    pactl set-sink-port alsa_output.pci-0000_00_1f.3.analog-stereo analog-output-speaker
}

function_headphones () {
    pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:analog-stereo
    pactl set-default-sink alsa_output.pci-0000_00_1f.3.analog-stereo
    pactl set-sink-port alsa_output.pci-0000_00_1f.3.analog-stereo analog-output-headphones
}

case "$1" in
    -0) function_hdmi ;;
    -1) function_dp1 ;;
    -2) function_dp2 ;;
    -l) function_lineout ;;
    -s) function_speaker ;;
    -h) function_headphones ;;
esac



# pacmd list-cards
# pacmd list-sinks

# pactl set-sink-volume @DEFAULT_SINK@ 100%
# pactl set-sink-volume @DEFAULT_SINK@ +10%
# pactl set-sink-volume @DEFAULT_SINK@ -10%
# pactl set-sink-mute @DEFAULT_SINK@ toggle

# wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
# wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%+
# wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%-
# wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
