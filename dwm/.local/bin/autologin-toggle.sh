#!/bin/sh

function_systemd () {
    if test -d /etc/systemd/system/getty@tty1.service.d; then
        sudo rm -rf /etc/systemd/system/getty@tty1.service.d && \
        echo "Autologin disabled."
    else
        sudo mkdir /etc/systemd/system/getty@tty1.service.d && \
        sudo sh -c "echo -e '[Service]\nExecStart=\nExecStart=-/sbin/agetty --noclear --autologin $(whoami) %I \$TERM' > \
        /etc/systemd/system/getty@tty1.service.d/autologin.conf" && \
        echo "Autologin enabled."
    fi
}

function_runit () {
    if grep -q "$(whoami)" /etc/runit/sv/agetty-tty1/conf; then
        sudo sed -i "s/ --autologin $(whoami)//" /etc/runit/sv/agetty-tty1/conf && \
        echo "Autologin disabled."
    else
        sudo sed -i "s/--noclear/--noclear --autologin $(whoami)/" /etc/runit/sv/agetty-tty1/conf && \
        echo "Autologin enabled."
    fi
}

if pidof systemd; then
    function_systemd
else
    function_runit
fi
