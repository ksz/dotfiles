#!/bin/sh

function_off () {
    pidof -s xautolock && xautolock -exit
    xset -q | grep -q "DPMS is Enabled" && xset s off -dpms
    # xset -display :0 s 0 0; xset -display :0 dpms 0 0 0
}

function_on () {
    pidof -s xautolock || (xautolock -time 20 -locker "$ctl suspend" -detectsleep) &
    xset -q | grep -q "DPMS is Enabled" || xset s on +dpms
    # xset -display :0 s 600 600; xset -display :0 dpms 600 600 600
}

function_toggle () {
    pidof -x idle-detect.sh && \
    pkill -TERM -P $(pidof -x idle-detect.sh) && dunstify -t 1000 "Sleep on idle disabled..." || \
    function_xinitrc && dunstify -t 1000 "Sleep on idle enabled..."
    xset -q | grep -q "DPMS is Enabled" || xset s on +dpms
}

function_xinitrc () {
    (while true; do
        grep -qr "RUNNING" /proc/asound && function_off || function_on
        # grep -q RUNNING /proc/asound/card*/pcm*/sub*/status && function_off || function_on
        sleep 300
    done) &
}

pidof systemd && ctl=systemctl || ctl=loginctl

case "$1" in
    -t) function_toggle ;;
    -x) function_xinitrc ;;
esac


# https://aur.archlinux.org/packages/xidlehook
