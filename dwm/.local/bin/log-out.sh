#!/bin/sh

if pidof systemd; then
    ctl=systemctl
else
    ctl=loginctl
fi

case "$1" in
    -p) $ctl poweroff ;;
    -r) $ctl reboot ;;
    -s) $ctl suspend ;;
    -l) slock -m "Locked $(date +'%Y-%m-%d') at $(date +'%H:%M:%S')" & (sleep 5.5 && xset dpms force off) ;;
    -e) killall dwm || openbox --exit ;;
    -d) sudo grub-reboot 2 && $ctl reboot ;;
    -h) kill $(pidof -x shutdown-schedule.sh) && dunstify -u critical -r 01 "log-out.sh" "System shutdown cancelled." || shutdown-schedule.sh ;;
esac
