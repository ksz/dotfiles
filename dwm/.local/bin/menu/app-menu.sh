#!/bin/sh

choices="fre:ac\nGParted\nKOReader\nVentoy"

chosen=$(echo -e "$choices" | dmenu -i -nf Cyan -p App:)

case "$chosen" in
    fre:ac  ) app-run.sh -f ;;
    GParted ) app-run.sh -g ;;
    KOReader) app-run.sh -k ;;
    Ventoy  ) app-run.sh -v ;;
esac
