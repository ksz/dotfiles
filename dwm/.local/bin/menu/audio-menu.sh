#!/bin/sh

choices="HDMI\nDP-1\nDP-2\nLineOut\nSpeaker\nHeadphones"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf Gold -p Audio:)
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 6 -i -p audiomenu)
fi

case "$chosen" in
    HDMI      ) audio-toggle.sh -0 ;;
    DP-1      ) audio-toggle.sh -1 ;;
    DP-2      ) audio-toggle.sh -2 ;;
    LineOut   ) audio-toggle.sh -l ;;
    Speaker   ) audio-toggle.sh -s ;;
    Headphones) audio-toggle.sh -h ;;
esac
