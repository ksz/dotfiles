#!/bin/sh

function_clipmenu () {
    if pgrep dwm >/dev/null; then
        clipmenu -i
    else
        CM_HISTLENGTH=15 CM_LAUNCHER=rofi clipmenu -i -p clipmenu
    fi
}

typeit=0
if [[ $1 == "--type" ]]; then
    typeit=1
    shift
fi

if [[ $typeit -eq 1 ]]; then
    function_clipmenu && sleep 0.3 && \
    xdotool type --clearmodifiers "$(xclip -o -selection clipboard)"
else
    function_clipmenu
fi
