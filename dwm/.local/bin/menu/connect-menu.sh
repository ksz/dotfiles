#!/bin/sh

choices="DietPi-SSH\nrAudio-SSH\nLibreELEC-SSH\nOSMC-SSH\nOpenWrt-SSH\nRouterOS-SSH\nDietPi-FISH\nrAudio-SFTP\n\
LibreELEC-SFTP\nOSMC-SFTP\nOpenWrt-FISH\nRouterOS-FISH\nLineageOS-FTP\nSimpleSSHD-SSH\nTermux-SSH\nDisroot-WebDAV"

chosen=$(echo -e "$choices" | dmenu -i -nf "Dodger Blue")

case "$chosen" in
    DietPi-SSH    ) st -e tmux new -s RPI0 ssh rpi0 ;;
    rAudio-SSH    ) st -e tmux new -s RPI2 ssh rpi2 ;;
    LibreELEC-SSH ) st -e tmux new -s RPI3 ssh rpi3 ;;
    OSMC-SSH      ) st -e tmux new -s RPI4 ssh rpi4 ;;
    OpenWrt-SSH   ) st -e tmux new -s LNKSS ssh lnkss ;;
    RouterOS-SSH  ) st -e tmux new -s MKRTK ssh mkrtk ;;
    DietPi-FISH   ) st -e mc sh://***@rpi0/home/dietpi ;;
    rAudio-SFTP   ) st -e mc sftp://***@rpi2 ;;
    LibreELEC-SFTP) st -e mc sftp://***@rpi3/storage ;;
    OSMC-SFTP     ) st -e mc sftp://***@rpi4/home/osmc ;;
    OpenWrt-FISH  ) st -e mc sh://***@lnkss/root ;;
    RouterOS-FISH ) st -e mc sh://***@mkrtk ;;   # FISH not working ???
    LineageOS-FTP ) st -e mc ftp://***:*****@*.*.*.*:* ;;
    SimpleSSHD-SSH) st -e tmux new -s SSSHD ssh ssshd ;;
    Termux-SSH    ) st -e tmux new -s TRMX ssh trmx ;;
    Disroot-WebDAV) echo Disroot ;;   # todo: davfs2 | rclone | cadaver ???
esac


# https://en.wikipedia.org/wiki/X11_color_names

# TODO:

# abduco or dtach instead of tmux ???
