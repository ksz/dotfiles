#!/bin/sh

choices="Shutdown\nReboot\nSuspend\nLock\nLogout\nDualboot\nSchedule"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf Tomato -p Exit:)
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 7 -i -p exitmenu)
fi

case "$chosen" in
    Shutdown) log-out.sh -p ;;
    Reboot  ) log-out.sh -r ;;
    Suspend ) log-out.sh -s ;;
    Lock    ) log-out.sh -l ;;
    Logout  ) log-out.sh -e ;;
    Dualboot) log-out.sh -d ;;
    Schedule) log-out.sh -h ;;
esac
