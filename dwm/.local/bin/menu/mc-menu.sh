#!/bin/sh

choices="rpi0\nrpi2\nrpi3\nrpi4\nlnkss\nmkrtk\nkndl\nlos"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf "Sky Blue" -p MC:)
    opt="-t $chosen:mc"
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 8 -i -p mcmenu)
    opt="-g 135x35 -t $chosen:mc"
fi

export SSH_AUTH_SOCK="$(find /tmp/ssh-*/agent.* -maxdepth 1)"

case "$chosen" in
    rpi0 ) st $opt -e mc -x sh://***@rpi0/*** ;;
    rpi2 ) st $opt -e mc -x sh://***@rpi2/*** ;;
    rpi3 ) st $opt -e mc -x sh://***@rpi3/*** ;;
    rpi4 ) st $opt -e mc -x sh://***@rpi4/*** ;;
    lnkss) st $opt -e mc -x sh://***@lnkss/root ;;
    mkrtk) st $opt -e mc -x sh://***@mkrtk/*** ;;   # FISH not working ???
    kndl ) st $opt -e mc -x sh://***@kndl/*** ;;
    los  ) st $opt -e mc -x ftp://***:*****@*.*.*.*:* ;;
esac
