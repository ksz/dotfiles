#!/bin/sh

choices="NAS\nUSB\nROM\nISO\nDualboot\nDisroot\nMEGA"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf "Lawn Green" -p Mount:)
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 7 -i -p mountmenu)
fi

case "$chosen" in
    NAS     ) mount-help.sh -n ;;
    USB     ) mount-help.sh -u ;;
    ROM     ) mount-help.sh -r ;;
    ISO     ) mount-help.sh -i ;;
    Dualboot) mount-help.sh -a ;;
    Disroot ) mount-help.sh -d ;;
    MEGA    ) mount-help.sh -m ;;
esac
