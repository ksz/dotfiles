#!/usr/bin/env bash

echo test | gpg --sign --armor --dry-run >/dev/null

shopt -s nullglob globstar

typeit=0
if [[ $1 == "--type" ]]; then
    typeit=1
    shift
fi

prefix=${PASSWORD_STORE_DIR-~/.password-store}
password_files=( "$prefix"/**/*.gpg )
password_files=( "${password_files[@]#"$prefix"/}" )
password_files=( "${password_files[@]%.gpg}" )

password=$(printf '%s\n' "${password_files[@]}" | dmenu -i "$@")

[[ -n $password ]] || exit

if [[ $typeit -eq 1 ]]; then
    pass show "$password" | sed -n "2{p;q}" | xdotool type --clearmodifiers --file - && \
    sleep 0.3 && xdotool key Tab && \
    pass show "$password" | { IFS= read -r pass; printf %s "$pass"; } | xdotool type --clearmodifiers --file - && \
    sleep 0.3 && xdotool key Return
else
    clipctl disable && \
    pass show "$password" | sed -n "3{p;q}" | xargs xdg-open
    pass show "$password" | sed -n "2{p;q}" | xsel
    pass show -c "$password" 2>/dev/null
    clipctl enable
fi


# script by ksz
# based on passmenu:
# https://git.zx2c4.com/password-store/tree/contrib/dmenu
# partly inspired by passmenu-plus:
# https://github.com/ciil/passmenu-plus
