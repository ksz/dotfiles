#!/bin/sh

choices="rpi0\nrpi2\nrpi3\nrpi4\nlnkss\nmkrtk\nkndl\nssshd\ntrmx"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf "Dodger Blue" -p SSH:)
    opt="-t $chosen:ssh"
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 9 -i -p sshmenu)
    opt="-g 135x35 -t $chosen:ssh"
fi

export SSH_AUTH_SOCK="$(find /tmp/ssh-*/agent.* -maxdepth 1)"

case "$chosen" in
    rpi0 ) st $opt -e tmux new -s RPI0 ssh rpi0 ;;
    rpi2 ) st $opt -e tmux new -s RPI2 ssh rpi2 ;;
    rpi3 ) st $opt -e tmux new -s RPI3 ssh rpi3 ;;
    rpi4 ) st $opt -e tmux new -s RPI4 ssh rpi4 ;;
    lnkss) st $opt -e tmux new -s LNKSS ssh lnkss ;;
    mkrtk) st $opt -e tmux new -s MKRTK ssh mkrtk ;;
    kndl ) st $opt -e tmux new -s KNDL ssh kndl ;;
    ssshd) st $opt -e tmux new -s SSSHD ssh ssshd ;;
    trmx ) st $opt -e tmux new -s TRMX ssh trmx ;;
esac
