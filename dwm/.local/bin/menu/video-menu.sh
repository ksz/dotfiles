#!/bin/sh

choices="HDMI\nDP-1\nDP-1+DP-2\nHDMI+DP-1+DP-2"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf Orange -p Display:)
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 4 -i -p videomenu)
fi

case "$chosen" in
    HDMI          ) video-toggle.sh -0 ;;
    DP-1          ) video-toggle.sh -1 ;;
    DP-1+DP-2     ) video-toggle.sh -2 ;;
    HDMI+DP-1+DP-2) video-toggle.sh -3 ;;
esac
