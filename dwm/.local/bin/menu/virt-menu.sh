#!/bin/sh

choices="Arch Linux\nDebian"

if pgrep dwm >/dev/null; then
    chosen=$(echo -e "$choices" | dmenu -i -nf "Lime Green" -p VM:)
else
    chosen=$(echo -e "$choices" | rofi -dmenu -l 2 -i -p virtmenu)
fi

case "$chosen" in
    "Arch Linux") virt-run.sh -a ;;
    Debian      ) virt-run.sh -d ;;
esac
