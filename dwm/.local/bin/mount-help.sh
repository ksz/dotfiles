#!/bin/sh

function_disroot () {
    # if df | grep -q /mnt/Disroot; then
    if mount -l | grep -q /mnt/Disroot; then
        # fusermount -uz ~/Mount/Disroot && \     # ???
        sudo fusermount -uz /mnt/Disroot && \
        dunstify "mount-help.sh" "Disroot unmounted."
    else
        # rclone mount disroot: ~/Mount/Disroot --vfs-cache-mode writes --allow-other --daemon && \     # ???
        sudo rclone mount disroot: /mnt/Disroot --vfs-cache-mode writes --allow-other --daemon && \
        dunstify "mount-help.sh" "Disroot mounted."
    fi
}

function_dualboot () {
    if mount -l | grep -q /mnt/Dualboot; then
        sudo umount /mnt/Dualboot/OS && \
        sudo umount /mnt/Dualboot/DATA && \
        dunstify "mount-help.sh" "Partition OS and DATA unmounted."
    else
        sudo mount /dev/disk/by-label/OS /mnt/Dualboot/OS && \
        sudo mount /dev/disk/by-label/DATA /mnt/Dualboot/DATA && \
        dunstify "mount-help.sh" "Partition OS and DATA mounted."
    fi
}

function_iso () {
    if mount -l | grep -q /mnt/ISO; then
        sudo umount /mnt/ISO && \
        dunstify "mount-help.sh" "ISO unmounted."
    else
        iso=$(basename -a ~/Downloads/*.iso | dmenu -p "Choose file:")
        sudo mount ~/Downloads/$iso /mnt/ISO -o loop && \
        dunstify "mount-help.sh" "$iso mounted."
    fi
}

function_mega () {
    # if df | grep -q /mnt/MEGA; then
    if mount -l | grep -q /mnt/MEGA; then
        # fusermount -uz ~/Mount/MEGA && \     # ???
        sudo fusermount -uz /mnt/MEGA && \
        dunstify "mount-help.sh" "MEGA unmounted."
    else
        # rclone mount disroot: ~/Mount/MEGA --vfs-cache-mode writes --allow-other --daemon && \     # ???
        sudo rclone mount disroot: /mnt/MEGA --vfs-cache-mode writes --allow-other --daemon && \
        dunstify "mount-help.sh" "MEGA mounted."
    fi
}

function_nas () {
    if mount -l | grep -q /mnt/NAS; then
        sudo umount /mnt/NAS && \
        dunstify "mount-help.sh" "NAS unmounted."
    else
        sudo mount -t nfs 192.168.*.*:/mnt/*** /mnt/NAS && \
        dunstify "mount-help.sh" "NAS mounted."
    fi
}

function_rom () {
    if mount -l | grep -q /mnt/ROM; then
        sudo umount /mnt/ROM && \
        dunstify "mount-help.sh" "ROM unmounted."
    else
        sudo mount /dev/sr0 /mnt/ROM && \
        dunstify "mount-help.sh" "ROM mounted."
    fi
}

function_usb () {
    if mount -l | grep -q /mnt/USB; then
        sudo umount /mnt/USB && \
        dunstify "mount-help.sh" "USB unmounted."
    else
        sudo mount /dev/sdb1 /mnt/USB && \
        dunstify "mount-help.sh" "USB mounted."
    fi
}

case "$1" in
    -d) function_disroot ;;
    -a) function_dualboot ;;
    -i) function_iso ;;
    -m) function_mega ;;
    -n) function_nas ;;
    -r) function_rom ;;
    -u) function_usb ;;
esac
