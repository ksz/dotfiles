#!/bin/bash

myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/$1/helpers"

if [ "$2" = "artix" ]; then
    sed -i "s/_Light/_Snow/;s/22/16/" ~/.{config/gtk-3.0/settings.ini,Xresources}
fi

xdg-user-dirs-update
ln -sf $myhelp/$2-wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
ln -sf $myhelp/$2-wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg

sudo chsh -s /bin/zsh "$myuser"

sudo mkdir /root/.config
sudo cp /home/$myuser/.gtkrc-2.0 /root
sudo cp -r /home/$myuser/.config/{fontconfig,gtk-3.0,mc,micro} /root/.config
# [ -d /home/$myuser/.config/qt5ct ] && \
# sudo cp -r /home/$myuser/.config/{qt5ct,qt6ct} /root/.config
[ -f /home/$myuser/.config/gtk-3.0/dark.settings.ini ] && \
sed -i "/dark.settings.ini/s/# //" ~/.xinitrc && \
sudo ln -sf /root/.config/gtk-3.0/light.settings.ini /root/.config/gtk-3.0/settings.ini
sudo sed -i "s/modarin256/modarin256root/" /root/.config/mc/ini
sudo sh -c "echo -e '\nexport EDITOR=\"micro\"' >> /etc/environment"

# xcursor-breeze artix fix
if [ "$2" = "artix" ]; then
    [ -d /usr/share/icons/Breeze_Snow/Breeze_Snow/cursors ] && \
    sudo mv /usr/share/icons/Breeze_Snow /usr/share/icons/Breeze_Snow.bak && \
    sudo ln -sf /usr/share/icons/Breeze_Snow.bak/Breeze_Snow /usr/share/icons/Breeze_Snow
fi
