#!/bin/sh

file=$HOME/Desktop/ss.$(date +'%Y%m%d.%H%M%S').png
sound=/usr/share/sounds/freedesktop/stereo/camera-shutter.oga

case $1 in
    -a) maim -ui $(xdotool getactivewindow) $file ;;
    -f) maim -u $file ;;
    -s) maim -us $file ;;
    # -a) scrot -u $file ;;
    # -f) scrot $file ;;
    # -s) scrot -s $file ;;
esac

if [ $? -eq 0 ]; then pw-cat -p $sound; fi
# if [ $? -eq 0 ]; then paplay $sound; fi

echo $file | xclip -selection clipboard
