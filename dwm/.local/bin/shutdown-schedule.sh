#!/bin/sh

if pgrep dwm >/dev/null; then
    timeout=$(dmenu -p "Enter timeout in minutes:" </dev/null)
else
    timeout=$(rofi -dmenu -l 0 -p "Enter timeout in minutes" </dev/null)
fi

if [ -z "$timeout" ] || [ -z "${timeout##*[!0-9]*}" ] || [ "$timeout" -lt 1 ]; then
    dunstify -u critical "shutdown-schedule.sh" "Incorrect timeout value. Try again."
    exit
fi

# TODO: turn off auto suspend

if pidof -q systemd; then
    ctl=systemctl
else
    ctl=loginctl
fi

timeout=$(($timeout*60))

for i in $(eval echo "{$timeout..1}"); do
    dunstify -r 01 "shutdown-schedule.sh" "System will shut down in ${i} seconds.\nTo abort choose 'Schedule' again."
    sleep 1
done

dunstify -r 01 "shutdown-schedule.sh" "The computer is about to shut down."
sleep 5
$ctl poweroff
