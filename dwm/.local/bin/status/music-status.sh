#!/bin/sh

function_xinitrc () {
    [ ! -f /tmp/status-music ] && touch /tmp/status-music
}

case $BUTTON in
    1) xdg-open https://last.fm/user/ksz16 ;;
    2) echo TODO ;;
    3) xdg-open https://listenbrainz.org/user/ksz16/ ;;
esac

case $1 in
    -x) function_xinitrc ;;
esac
