#!/bin/sh

function_xinitrc () {
    if [[ ! -e /tmp/status-packages ]] || [[ `find /tmp/status-packages -mmin +300` ]]; then
        (sleep 5 && sudo pacman -Syy >/dev/null
        pacmanpkgs=$(pacman -Qu | grep -Fcv "\[ignored\]")
        yaypkgs=$(yay -Qu --aur | grep -Fcv "\[ignored\]")
        echo "$pacmanpkgs/$yaypkgs" > /tmp/status-packages) &
    fi
}

case $BUTTON in
    1) st -e sudo pacman -Syyu && sed -i "s/^.*\//0\//" /tmp/status-packages ;;
    2) echo TODO ;;
    3) st -e yay -Syu --aur --sudoloop && sed -i "s/\/.*$/\/0/" /tmp/status-packages ;;
esac

case $1 in
    -x) function_xinitrc ;;
esac
