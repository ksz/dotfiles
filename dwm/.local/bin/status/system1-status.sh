#!/bin/sh

case $BUTTON in
    1) pgrep conky && killall conky || st -c "statuspad" -g "50x35-20+35" -e conky -c ~/.config/conky/term.conf ;;
    2) echo TODO ;;
    3) st -c "statuspad" -g "80x25-20+35" -e sh -c "neofetch; read _" ;;
esac

# pidof -x system1-status.sh && pkill -TERM -P $(pidof -x system1-status.sh)
