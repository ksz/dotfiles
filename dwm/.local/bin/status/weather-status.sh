#!/bin/sh

function_xinitrc () {
    if [[ ! -e /tmp/status-weather ]] || [[ `find /tmp/status-weather -mmin +30` ]]; then
        (while true; do curl -s wttr.in/?format="%t+%w" > /tmp/status-weather; sleep 3600; done) &
    else
        (while sleep 3000; do curl -s wttr.in/?format="%t+%w" > /tmp/status-weather; done) &
    fi
}

case $BUTTON in
    1) st -c "statuspad" -g "130x40-20+35" -e sh -c "curl wttr.in; read _" ;;
    2) echo TODO ;;
    3) xdg-open https://duckduckgo.com/?q=pogoda+krakow&t=ffab&ia=weather ;;
esac

case $1 in
    -x) function_xinitrc ;;
esac


# pidof -x weather-status.sh && pkill -TERM -P $(pidof -x weather-status.sh)
# OR wmctrl OR xdotool search ???
