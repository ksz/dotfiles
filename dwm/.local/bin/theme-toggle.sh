#!/bin/sh

if cat ~/.config/gtk-3.0/settings.ini | grep -q "Arc-Dark"; then
    sed -i "/gtk-theme-name/s/Dark/Lighter/" ~/.config/gtk-3.0/settings.ini
    sed -i "/gtk-icon-theme-name/s/Dark/Light/" ~/.config/gtk-3.0/settings.ini
    dunstify -t 1000 "Theme: Arc-Lighter..."
else
    sed -i "/gtk-theme-name/s/Lighter/Dark/" ~/.config/gtk-3.0/settings.ini
    sed -i "/gtk-icon-theme-name/s/Light/Dark/" ~/.config/gtk-3.0/settings.ini
    dunstify -t 1000 "Theme: Arc-Dark..."
fi

# wmctrl -c Firefox && sleep 1 && firefox
