#!/bin/sh

function_hdmi () {
    xrandr --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --off --output HDMI-2 --off --output DP-2 --off --output HDMI-3 --off --output DP-3 --off
    xwallpaper --clear --center ~/Pictures/wallpaper.jpg
    audio-toggle.sh -0
}

function_dp1 () {
    xrandr --output HDMI-1 --off --output DP-1 --primary --mode 1920x1200 --pos 0x0 --rotate normal --output HDMI-2 --off --output DP-2 --off --output HDMI-3 --off --output DP-3 --off
    xwallpaper --clear --center ~/Pictures/wallpaper.jpg
    audio-toggle.sh -1
}

function_dp1dp2 () {
    xrandr --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output DP-2 --mode 1920x1080 --pos 1920x0 --rotate left --output HDMI-3 --off --output DP-3 --off
    sleep 0.5
    xrandr --output HDMI-1 --off --output DP-1 --primary --mode 1920x1200 --pos 0x720 --rotate normal --output HDMI-2 --off --output DP-2 --mode 1920x1080 --pos 1920x0 --rotate left --output HDMI-3 --off --output DP-3 --off
    xwallpaper --clear --output DP-1 --center ~/Pictures/wallpaper.jpg --output DP-2 --center ~/Pictures/wallpaper-rotate.jpg
    xdotool mousemove --clearmodifiers 100 1000
    sleep 0.5
    xdotool click --clearmodifiers 1
    audio-toggle.sh -1
}

function_hdmidp1dp2 () {
    xrandr --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output DP-2 --mode 1920x1080 --pos 1920x0 --rotate left --output HDMI-3 --off --output DP-3 --off
    sleep 0.5
    xrandr --output HDMI-1 --mode 1920x1080 --pos 0x840 --rotate normal --output DP-1 --primary --mode 1920x1200 --pos 1920x720 --rotate normal --output HDMI-2 --off --output DP-2 --mode 1920x1080 --pos 3840x0 --rotate left --output HDMI-3 --off --output DP-3 --off
    xwallpaper --clear --output HDMI-1 --center ~/Pictures/wallpaper.jpg --output DP-1 --center ~/Pictures/wallpaper.jpg --output DP-2 --center ~/Pictures/wallpaper-rotate.jpg
    xdotool mousemove --clearmodifiers 2000 1000
    sleep 0.5
    xdotool click --clearmodifiers 1
    audio-toggle.sh -1
}

function_qemu () {
    xrandr --output Virtual-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output Virtual-2 --off --output Virtual-3 --off --output Virtual-4 --off
    xwallpaper --clear --center ~/Pictures/wallpaper.jpg
}

function_vbox () {
    xwallpaper --clear --center ~/Pictures/wallpaper.jpg
}

case "$1" in
    -0) function_hdmi ;;
    -1) function_dp1 ;;
    -2) function_dp1dp2 ;;
    -3) function_hdmidp1dp2 ;;
    -q) function_qemu ;;
    -v) function_vbox ;;
esac
