#!/bin/sh

function_status () {
    if wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q "\[MUTED\]"; then
        echo M > /tmp/status-volume
        dunstify -u low -t 3000 -r 02 "volume-set.sh" "Volume: MUTE"
    else
        vol=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | sed "s/Volume: //")
        vol=$(echo "scale=0;($vol*100)/1" | bc)
        echo $vol > /tmp/status-volume
        dunstify -u low -t 3000 -r 02 "volume-set.sh" "Volume: $vol%"
    fi
}

case $1 in
    -d) wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%- ;;
    -i) wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 10%+ ;;
    -f) wpctl set-volume @DEFAULT_AUDIO_SINK@ 100% ;;
    -m) wpctl set-mute @DEFAULT_AUDIO_SINK@ 1 ;;
    -u) wpctl set-mute @DEFAULT_AUDIO_SINK@ 0 ;;
    -t) wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle ;;
esac

if [ $? -eq 0 ]; then function_status; fi



# function_slstatus () {
    # if pgrep slstatus >/dev/null; then
        # if amixer sget Master | grep -q "\[off\]"; then
            # echo M > /tmp/status-volume
        # else
            # amixer sget Master | tail -1 | awk '{print $5}' | sed "s/^.//;s/.$//" > /tmp/status-volume
        # fi
    # fi
# }

# case $1 in
    # -d) amixer sset Master 10%- ;;
    # -i) amixer sset Master 10%+ ;;
    # -f) amixer sset Master 100% ;;
    # -m) amixer sset 'Auto-Mute Mode' Enabled ;;
    # -u) amixer sset 'Auto-Mute Mode' Disabled ;;
    # -t) amixer sset Master toggle ;;
# esac

# if [ $? -eq 0 ]; then function_slstatus; fi
