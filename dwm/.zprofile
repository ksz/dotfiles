typeset -U path PATH
path=($path $(find $HOME/.local/bin -type d | awk '{printf "%s ", $0}'))
export PATH

export TERMINAL="st"
export EDITOR="micro"
export BROWSER="firefox"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export QT_QPA_PLATFORMTHEME="qt5ct"
# export _JAVA_AWT_WM_NONREPARENTING=1
# export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=lcd'

export SUDO_ASKPASS="$HOME/.local/bin/menu/secret-menu.sh"
export CM_SELECTIONS="clipboard"
export CM_IGNORE_WINDOW="KeePassXC"     # TODO: QtPass regex ???

if [ -z "${DISPLAY}" ] && [ "$(tty)" = "/dev/tty1" ]; then
    echo -e "\nChoose window manager:"
    echo "  1) dwm 2) openbox *) default 0) tty"
    read mywm\?"Enter a number: "
    case "$mywm" in
        0) echo ;;
        1) exec startx $HOME/.xinitrc dwm ;;
        2) exec startx $HOME/.xinitrc obx ;;
        *) exec startx $HOME/.xinitrc ;;
    esac
fi
