export ZSH="$HOME/.oh-my-zsh"
export UPDATE_ZSH_DAYS=30

ZSH_THEME="agnoster"
HIST_STAMPS="yyyy-mm-dd"

plugins=(
  fast-syntax-highlighting
  zsh-autosuggestions
  ohmyzsh-full-autoupdate
  git
  copyfile
  copypath
  sudo
)

cdpath=($HOME/.config /mnt)

fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src

source $ZSH/oh-my-zsh.sh

alias \
  ping='ping -c 5' \
  sudo='sudo ' \
  wget='wget -c' \
  bc='bc -lq' \
  df='df -h' \
  du='du -ah' \
  ht='htop -t' \
  ka='killall' \
  mc='mc -x' \
  mi='micro' \
  nn='nano -l' \
  za='zathura' \
  cp='cp -iv' \
  mv='mv -iv' \
  rm='rm -Iv' \
  xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS" && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""' \
  xx='exa -abghHliS --group-directories-first --time-style long-iso'

prompt_context() {
  if [[ "$USERNAME" != "$DEFAULT_USER" || -n "$SSH_CLIENT" ]]; then
    prompt_segment green default "%(!.%{%F{yellow}%}.)%n@%m"
  fi
}

eval $(thefuck --alias ff)
