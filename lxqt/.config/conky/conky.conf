conky.config = {
    alignment = 'top_right',
    background = false,
    border_width = 2,
    cpu_avg_samples = 2,
    default_color = 'white',
    default_outline_color = 'white',
    default_shade_color = 'white',
    double_buffer = true,
    draw_borders = true,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    extra_newline = false,
    -- font = 'Hack:size=9',
    font = 'Noto Sans:bold:size=8',
    gap_x = 25,
    gap_y = 30,
    maximum_width = 210,
    minimum_width = 210,
    minimum_height = 310,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_colour = '29292b',
    own_window_hints = 'undecorated,below,skip_taskbar,sticky,skip_pager',
    own_window_transparent = false,
    own_window_type = 'normal',
    own_window_argb_visual = false,
    own_window_argb_value = 0,
    show_graph_range = false,
    show_graph_scale = false,
    stippled_borders = 0,
    total_run_times = 0,
    update_interval = 1.0,
    uppercase = false,
    use_spacer = 'none',
    use_xft = true,
    xftalpha = 1.0,
}

conky.text = [[
$nodename  -  $sysname $kernel
$hr
${color grey}Uptime:$color $uptime
${color grey}Frequency (in MHz):$color $freq
${color grey}Frequency (in GHz):$color $freq_g
${color grey}RAM Usage:$color $mem/$memmax - $memperc% ${membar 4}
${color grey}Swap Usage:$color $swap/$swapmax - $swapperc% ${swapbar 4}
${color grey}CPU Usage:$color $cpu% ${cpubar 4}
${color grey}CPU Temperature:$color ${hwmon 3 temp 2}°C   ${hwmon 3 temp 4}°C
# /sys/class/hwmon/*
${color grey}Available Entropy:$color $entropy_avail
${color grey}Processes:$color $processes  ${color grey}Running:$color $running_processes
$hr
${color grey}File systems:
${color grey}  ROOT:$color ${fs_used /}/${fs_size /} ${fs_bar 6 /}
${color grey}Networking:
${color grey}  Up:$color ${upspeed ens5f5}${color grey} - Down:$color ${downspeed ens5f5}
${color grey}  Local IP:$color ${execi 43200 ip -o -4 addr list ens5f5 | awk '{print $4}' | cut -d/ -f1}
${color grey}  Public IP:$color ${execi 43200 curl -s ifconfig.me}
$hr
${color grey}Weather:
${color grey}  Bochnia:$color ${texeci 3600 bash weather-show.sh -b}
${color grey}  Kraków:$color ${texeci 3600 bash weather-show.sh -k}
# ${color grey}Date/Time:
# ${color grey}  Europe/Warsaw:$color  ${time %F  %T}
]]
