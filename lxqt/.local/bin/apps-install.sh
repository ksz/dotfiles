#!/bin/bash

mydistro=$(grep -o -P '(?<=^ID=).*(?=$)' /etc/os-release)

function_help () {
    echo "  -a,  --anydesk              install/upgrade AnyDesk"
    echo "  -b,  --balenaetcher         install/upgrade balenaEtcher"
    echo "  -db, --dbeaver              install/upgrade DBeaver"
    echo "  -d,  --deadbeef             install/upgrade DeaDBeeF"
    echo "  -e,  --eget                 install/upgrade Eget"
    echo "  -o,  --fdroidcl             install/upgrade fdroidcl"
    echo "  -f,  --freac                install/upgrade fre:ac"
    echo "  -ft, --freetube             install/upgrade FreeTube"
    echo "  -k,  --koreader             install/upgrade KOReader"
    echo "  -l,  --librewolf            install/upgrade LibreWolf"
    echo "  -m,  --mpv                  install/upgrade mpv scripts"
    echo "  -p,  --python               install/upgrade flexget/napi-py/subliminal"
    echo "  -r,  --rustdesk             install/upgrade RustDesk"
    echo "  -t,  --tinymediamanager     install/upgrade tinyMediaManager"
    echo "  -v,  --vscodium             install/upgrade VSCodium"
    echo "  -x,  --winbox               install/upgrade WinBox"
    echo "  -w,  --windscribe           install/upgrade Windscribe"
}

function_anydesk () {
    if [ "$mydistro" = "arch" ]; then
        yay -S --noconfirm --needed anydesk-bin
    elif [ "$mydistro" = "debian" ]; then
        myver=$(curl -s https://download.anydesk.com/linux/ | grep amd64.deb\" | grep -o -P '(?<=anydesk_).*(?=")' | sort -V | tail -n 1)
        wget -O ~/anydesk.deb https://download.anydesk.com/linux/anydesk_$myver
        sudo dpkg -i ~/anydesk.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        myver=$(curl -s https://download.anydesk.com/linux/ | grep el8.x86_64.rpm\" | grep -o -P '(?<=anydesk-).*(?=")' | sort -V | tail -n 1)
        wget -O ~/anydesk.rpm https://download.anydesk.com/linux/anydesk-$myver
        sudo dnf -y install ~/anydesk.rpm
    fi
    rm -f ~/anydesk.*
    sudo systemctl disable anydesk.service
    cp -f /etc/xdg/autostart/anydesk*.desktop ~/.config/autostart
    for i in $(ls ~/.config/autostart/anydesk*.desktop); do echo "Hidden=true" >> $i; done
}

function_balenaetcher () {
    # # myver=$(curl -s https://api.github.com/repos/balena-io/etcher/releases/latest | grep -oP '"tag_name": "v\K(.*)(?=")')
    if [ "$mydistro" = "arch" ]; then
        yay -S --noconfirm --needed etcher-bin
    elif [ "$mydistro" = "debian" ]; then
        eget balena-io/etcher -d -a amd64.deb --to ~/balena-etcher.deb
        # wget -O ~/balena-etcher.deb $(curl -sL https://api.github.com/repos/balena-io/etcher/releases/latest | jq --raw-output .assets[1].browser_download_url)
        # # wget -O ~/balena-etcher.deb https://github.com/balena-io/etcher/releases/download/v$myver/balena-etcher_"$myver"_amd64.deb
        sudo dpkg -i ~/balena-etcher.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        eget balena-io/etcher -d -a x86_64.rpm --to ~/balena-etcher.rpm
        # wget -O ~/balena-etcher.rpm $(curl -sL https://api.github.com/repos/balena-io/etcher/releases/latest | jq --raw-output .assets[0].browser_download_url)
        # # wget -O ~/balena-etcher.rpm https://github.com/balena-io/etcher/releases/download/v$myver/balena-etcher-$myver.x86_64.rpm
        sudo dnf -y install ~/balena-etcher.rpm
    fi
    rm -f ~/balena-etcher.*
}

function_dbeaver () {
    if [ "$mydistro" = "arch" ]; then
        sudo pacman -S --noconfirm --needed dbeaver
    elif [ "$mydistro" = "debian" ]; then
        wget -O ~/dbeaver.deb https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb
        sudo dpkg -i ~/dbeaver.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        wget -O ~/dbeaver.rpm https://dbeaver.io/files/dbeaver-ce-latest-stable.x86_64.rpm
        sudo dnf -y install ~/dbeaver.rpm
    fi
    rm -f ~/dbeaver.*
}

function_deadbeef () {
    myver=$(curl -sL https://sourceforge.net/projects/deadbeef/best_release.json | jq .platform_releases.linux.filename | cut -d \/ -f 4)
    # myver=$(curl -qsL "https://sourceforge.net/projects/deadbeef/best_release.json" | sed "s/, /,\n/g" | sed -rn "/release/,/\}/{ /filename/{ 0,//s/([^0-9]*)([0-9\.]+)([^0-9]*.*)/\2/ p }}")
    if [ "$mydistro" = "arch" ]; then
        wget -t 10 -O ~/deadbeef.pkg.tar.xz https://sourceforge.net/projects/deadbeef/files/travis/linux/$myver/deadbeef-static-$myver-1-x86_64.pkg.tar.xz/download
        sudo pacman -U --noconfirm ~/deadbeef.pkg.tar.xz
    elif [ "$mydistro" = "debian" ]; then
        wget -t 10 -O ~/deadbeef.deb https://sourceforge.net/projects/deadbeef/files/travis/linux/$myver/deadbeef-static_$myver-1_amd64.deb/download
        sudo dpkg -i ~/deadbeef.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        sudo dnf -y install deadbeef
    fi
    rm -f ~/deadbeef.*
}

function_eget () {
    if test -f /usr/local/bin/eget; then
        sudo eget zyedidia/eget --upgrade-only -a linux_amd64.tar.gz --to /usr/local/bin/eget
    else
        curl https://zyedidia.github.io/eget.sh | sh
        sudo mv eget /usr/local/bin && sudo chown root:root /usr/local/bin/eget
    fi
}

function_fdroidcl () {
    sudo eget mvdan/fdroidcl -d --upgrade-only -a linux_amd64 -a ^.gz --to /usr/local/bin/fdroidcl
    # sudo wget -O /usr/local/bin/fdroidcl $(curl -sL https://api.github.com/repos/mvdan/fdroidcl/releases/latest | jq --raw-output .assets[6].browser_download_url)
    # # myver=$(curl -s https://api.github.com/repos/mvdan/fdroidcl/releases/latest | grep -oP '"tag_name": "v\K(.*)(?=")')
    # # sudo wget -O /usr/local/bin/fdroidcl https://github.com/mvdan/fdroidcl/releases/download/v$myver/fdroidcl_v"$myver"_linux_amd64
    sudo chmod +x /usr/local/bin/fdroidcl
}

function_freac () {
    sudo eget enzo1982/freac -d --upgrade-only -a x86_64.AppImage --to /opt/freac.AppImage
    # sudo wget -O /opt/freac.AppImage $(curl -sL https://api.github.com/repos/enzo1982/freac/releases/latest | jq --raw-output .assets[5].browser_download_url)
    # # myver=$(curl -s https://api.github.com/repos/enzo1982/freac/releases/latest | grep -oP '"tag_name": "v\K(.*)(?=")')
    # # sudo wget -O /opt/freac.AppImage https://github.com/enzo1982/freac/releases/download/v$myver/freac-$myver-linux-x86_64.AppImage
    sudo chmod a+x /opt/freac.AppImage
    sed -i "/^Hidden=/s/true/false/" ~/.local/share/applications/freac.desktop
}

function_freetube () {
    if [ "$mydistro" = "arch" ]; then
        eget FreeTubeApp/FreeTube --pre-release -d -a amd64.pacman --to ~/freetube.pacman
        sudo pacman -U --noconfirm ~/freetube.pacman
    elif [ "$mydistro" = "debian" ]; then
        eget FreeTubeApp/FreeTube --pre-release -d -a amd64.deb --to ~/freetube.deb
        sudo dpkg -i ~/freetube.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        eget FreeTubeApp/FreeTube --pre-release -d -a amd64.rpm --to ~/freetube.rpm
        sudo dnf -y install ~/freetube.rpm
    fi
    rm -f ~/freetube.*
    sed -i "/^Hidden=/s/true/false/" ~/.local/share/applications/freetube.desktop
}

function_koreader () {
    sudo eget koreader/koreader -d --upgrade-only -a .AppImage --to /opt/koreader.AppImage
    # sudo wget -O /opt/koreader.AppImage $(curl -sL https://api.github.com/repos/koreader/koreader/releases/latest | jq --raw-output .assets[7].browser_download_url)
    # # myver=$(curl -s https://api.github.com/repos/koreader/koreader/releases/latest | grep -oP '"tag_name": "v\K(.*)(?=")')
    # # sudo wget -O /opt/koreader.AppImage https://github.com/koreader/koreader/releases/download/v$myver/koreader-appimage-x86_64-linux-gnu-v$myver.AppImage
    sudo chmod a+x /opt/koreader.AppImage
    sed -i "/^Hidden=/s/true/false/" ~/.local/share/applications/koreader.desktop
    if ! test -f /opt/koreader.svg; then sudo wget -O /opt/koreader.svg https://raw.githubusercontent.com/koreader/koreader/master/resources/koreader.svg; fi
    sed -i "/^Icon=/s/bookreader/\/opt\/koreader.svg/" ~/.local/share/applications/koreader.desktop
}

function_librewolf () {
    myver=$(curl -s https://gitlab.com/librewolf-community/browser/appimage/-/releases/permalink/latest | grep -o -P '(?<=releases/v).*(?=")')
    sudo wget -O /opt/librewolf.AppImage https://gitlab.com/api/v4/projects/24386000/packages/generic/librewolf/$myver/LibreWolf.x86_64.AppImage
    sudo chmod a+x /opt/librewolf.AppImage
    sed -i "/^Hidden=/s/true/false/" ~/.local/share/applications/librewolf.desktop
}

function_mpv () {
    if ! test -d ~/.config/mpv/scripts; then mkdir -p ~/.config/mpv/scripts; fi
    eget gaesa/mpv-autoload -d --upgrade-only -a .js --to ~/.config/mpv/scripts/autoload.js
    eget eNV25/mpv-mpris2 -d --upgrade-only -a .so --to ~/.config/mpv/scripts/mpris.so
}

function_python () {
    for i in flexget subliminal; do
        if test -d ~/.$i; then
            ~/.$i/bin/pip install --upgrade pip # setuptools wheel
            ~/.$i/bin/pip install --upgrade $i
        else
            python -m venv ~/.$i
            ~/.$i/bin/pip install --upgrade pip # setuptools wheel
            ~/.$i/bin/pip install $i
            ln -sf ~/.$i/bin/$i ~/.local/bin
        fi
    done
    if ! test -d ~/.napi-py; then
        python -m venv ~/.napi-py
        ~/.napi-py/bin/pip install --upgrade pip
        ~/.napi-py/bin/pip install napi-py==1.1.1
        ln -sf ~/.napi-py/bin/napi-py ~/.local/bin
    fi
}

function_rustdesk () {
    # # myver=$(curl -s https://api.github.com/repos/rustdesk/rustdesk/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")')
    if [ "$mydistro" = "arch" ]; then
        eget rustdesk/rustdesk -d -a .pkg.tar.zst --to ~/rustdesk.pkg.tar.zst
        # wget -O ~/rustdesk.pkg.tar.zst $(curl -sL https://api.github.com/repos/rustdesk/rustdesk/releases/latest | jq --raw-output .assets[12].browser_download_url)
        # # wget -O ~/rustdesk.pkg.tar.zst https://github.com/rustdesk/rustdesk/releases/download/$myver/rustdesk-$myver-x86_64.pkg.tar.zst
        sudo pacman -U --noconfirm ~/rustdesk.pkg.tar.zst
    elif [ "$mydistro" = "debian" ]; then
        eget rustdesk/rustdesk -d -a x86_64.deb --to ~/rustdesk.deb
        # wget -O ~/rustdesk.deb $(curl -sL https://api.github.com/repos/rustdesk/rustdesk/releases/latest | jq --raw-output .assets[8].browser_download_url)
        # # wget -O ~/rustdesk.deb https://github.com/rustdesk/rustdesk/releases/download/$myver/rustdesk-$myver-x86_64.deb
        sudo dpkg -i ~/rustdesk.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        eget rustdesk/rustdesk -d -a x86_64.rpm --to ~/rustdesk.rpm
        # wget -O ~/rustdesk.rpm $(curl -sL https://api.github.com/repos/rustdesk/rustdesk/releases/latest | jq --raw-output .assets[16].browser_download_url)
        # # wget -O ~/rustdesk.rpm https://github.com/rustdesk/rustdesk/releases/download/$myver/rustdesk-$myver.x86_64.rpm
        sudo dnf -y install ~/rustdesk.rpm
    fi
    rm -f ~/rustdesk.*
    sudo systemctl disable rustdesk.service
}

function_tinymediamanager () {
    myver=$(curl -s https://gitlab.com/tinyMediaManager/tinyMediaManager/-/releases/permalink/latest | grep -o -P '(?<=tinyMediaManager-).*(?=")')
    # wget -O ~/tinymediamanager.tar.gz https://archive.tinymediamanager.org/v$myver/tmm_"$myver"_linux-amd64.tar.gz
    # tar -xzf ~/tinymediamanager.tar.gz -C ~/
    wget -O ~/tinymediamanager.tar.xz https://archive.tinymediamanager.org/v$myver/tinyMediaManager-$myver-linux-amd64.tar.xz
    tar -xf ~/tinymediamanager.tar.xz -C ~/
    if test -d ~/.tinymediamanager; then
        rsync -a ~/tinyMediaManager/ ~/.tinymediamanager
        rm -rf ~/tinyMediaManager
    else
        mv ~/tinyMediaManager ~/.tinymediamanager
    fi
    rm -f ~/tinymediamanager.tar.*
    sed -i "/^Hidden=/s/true/false/" ~/.local/share/applications/tinymediamanager.desktop
}

function_vscodium () {
    # # myver=$(curl -s https://api.github.com/repos/VSCodium/vscodium/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")')
    if [ "$mydistro" = "arch" ]; then
        yay -S --noconfirm --needed vscodium-bin
    elif [ "$mydistro" = "debian" ]; then
        eget VSCodium/vscodium -d -a amd64.deb -a ^.sha --to ~/codium.deb
        # wget -O ~/codium.deb $(curl -sL https://api.github.com/repos/VSCodium/vscodium/releases/latest | jq --raw-output .assets[9].browser_download_url)
        # # wget -O ~/codium.deb https://github.com/VSCodium/vscodium/releases/download/$myver/codium_"$myver"_amd64.deb
        sudo dpkg -i ~/codium.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        eget VSCodium/vscodium -d -a x86_64.rpm -a ^.sha --to ~/codium.rpm
        # wget -O ~/codium.rpm $(curl -sL https://api.github.com/repos/VSCodium/vscodium/releases/latest | jq --raw-output .assets[6].browser_download_url)
        # # wget -O ~/codium.rpm https://github.com/VSCodium/vscodium/releases/download/$myver/codium-$myver-el7.x86_64.rpm
        sudo dnf -y install ~/codium.rpm
    fi
    if ! test -d ~/.vscode-oss/extensions/llacoste2000.unofficial-gitlab-dark-theme-*; then
        wget -O ~/llacoste2000.unofficial-gitlab-dark-theme.vsix https://gitlab.com/ksz/setup/-/raw/main/fedora/helpers/llacoste2000.unofficial-gitlab-dark-theme-1.2.1.vsix
        codium --install-extension ~/llacoste2000.unofficial-gitlab-dark-theme.vsix
    fi
    rm -f ~/codium.* ~/*.vsix
}

function_winbox () {
    wget -O ~/winbox.zip $(curl -s https://mikrotik.com/download | grep "WinBox_Linux" | cut -d \" -f2)
    sudo unzip -j "winbox.zip" "WinBox" -d "/opt"
    sudo mv /opt/WinBox /opt/winbox
    sudo chmod a+x /opt/winbox
    rm -f ~/winbox.zip
    sed -i "/^Hidden=/s/true/false/" ~/.local/share/applications/winbox.desktop
}

function_windscribe () {
    # # # myver=$(curl -s https://api.github.com/repos/Windscribe/Desktop-App/releases/latest | grep -oP '"tag_name": "v\K(.*)(?=")')
    if [ "$mydistro" = "arch" ]; then
        wget -O ~/windscribe.pkg.tar.zst https://windscribe.com/install/desktop/linux_zst_x64
        # eget Windscribe/Desktop-App -d -a .pkg.tar.zst --to ~/windscribe.pkg.tar.zst
        # # wget -O ~/windscribe.pkg.tar.zst $(curl -sL https://api.github.com/repos/Windscribe/Desktop-App/releases/latest | jq --raw-output .assets[5].browser_download_url)
        # # # wget -O ~/windscribe.pkg.tar.zst https://github.com/Windscribe/Desktop-App/releases/download/v$myver/windscribe_"$myver"_x86_64.pkg.tar.zst
        sudo pacman -U --noconfirm ~/windscribe.pkg.tar.zst
    elif [ "$mydistro" = "debian" ]; then
        wget -O ~/windscribe.deb https://windscribe.com/install/desktop/linux_deb_x64
        # eget Windscribe/Desktop-App -d -a .deb -a ^arm --to ~/windscribe.deb
        # # wget -O ~/windscribe.deb $(curl -sL https://api.github.com/repos/Windscribe/Desktop-App/releases/latest | jq --raw-output .assets[2].browser_download_url)
        # # # wget -O ~/windscribe.deb https://github.com/Windscribe/Desktop-App/releases/download/v$myver/windscribe_"$myver"_amd64.deb
        sudo dpkg -i ~/windscribe.deb
        sudo apt-get -yf install
    elif [ "$mydistro" = "fedora" ]; then
        wget -O ~/windscribe.rpm https://windscribe.com/install/desktop/linux_rpm_x64
        # eget Windscribe/Desktop-App -d -a .rpm --to ~/windscribe.rpm
        # # wget -O ~/windscribe.rpm $(curl -sL https://api.github.com/repos/Windscribe/Desktop-App/releases/latest | jq --raw-output .assets[6].browser_download_url)
        # # # wget -O ~/windscribe.rpm https://github.com/Windscribe/Desktop-App/releases/download/v$myver/windscribe_"$myver"_x86_64.rpm
        sudo dnf -y install ~/windscribe.rpm
    fi
    rm -f ~/windscribe.*
    # sudo systemctl disable windscribe-helper.service
}

if ! test -f /usr/local/bin/eget; then echo -e "\nEget is not installed\n"; fi

case "$1" in
    -a |--anydesk         ) function_anydesk ;;
    -b |--balenaetcher    ) function_balenaetcher ;;
    -db|--dbeaver         ) function_dbeaver ;;
    -d |--deadbeef        ) function_deadbeef ;;
    -e |--eget            ) function_eget ;;
    -o |--fdroidcl        ) function_fdroidcl ;;
    -f |--freac           ) function_freac ;;
    -ft|--freetube        ) function_freetube ;;
    -k |--koreader        ) function_koreader ;;
    -l |--librewolf       ) function_librewolf ;;
    -m |--mpv             ) function_mpv ;;
    -p |--python          ) function_python ;;
    -r |--rustdesk        ) function_rustdesk ;;
    -t |--tinymediamanager) function_tinymediamanager ;;
    -v |--vscodium        ) function_vscodium ;;
    -x |--winbox          ) function_winbox ;;
    -w |--windscribe      ) function_windscribe ;;
    *                     ) function_help ;;
esac

if [ "$DESKTOP_SESSION" = "lxqt" ]; then update-desktop-database ~/.local/share/applications; fi
