#!/bin/bash

### SDDM
myuser2=$(grep 1001 /etc/passwd | cut -d \: -f 1)
if grep -q "^User=" /etc/sddm.conf.d/sddm.conf; then
    sudo sed -i "/^User=/s/^/#/" /etc/sddm.conf.d/sddm.conf && \
    sudo sed -i "/^Session=/s/^/#/" /etc/sddm.conf.d/sddm.conf && \
    echo "Autologin for user \"$myuser2\" disabled."
else
    sudo sed -i "/^#User=/s/^.*$/User=$myuser2/" /etc/sddm.conf.d/sddm.conf && \
    sudo sed -i "/^#Session=/s/^#//" /etc/sddm.conf.d/sddm.conf && \
    echo "Autologin for user \"$myuser2\" enabled."
fi



### LightDM
# myuser2=$(grep 1001 /etc/passwd | cut -d \: -f 1)
# if test -d /etc/lightdm/lightdm.conf.d; then
#     sudo rm -rf /etc/lightdm/lightdm.conf.d && \
#     echo "Autologin for user \"$myuser2\" disabled."
# else
#     sudo mkdir /etc/lightdm/lightdm.conf.d && \
#     sudo cp /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.d && \
#     sudo sed -i "/^#greeter-hide-users=false/s/^#//" /etc/lightdm/lightdm.conf.d/lightdm.conf && \
#     sudo sed -i "s/^#autologin-user=/autologin-user=$myuser2/" /etc/lightdm/lightdm.conf.d/lightdm.conf && \
#     sudo sed -i "/^#autologin-user-timeout=0/s/^#//" /etc/lightdm/lightdm.conf.d/lightdm.conf && \
#     echo "Autologin for user \"$myuser2\" enabled."
# fi
