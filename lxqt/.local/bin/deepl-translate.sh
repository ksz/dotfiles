#!/bin/bash

if xdotool search --onlyvisible --classname "Navigator"; then
    xclip -out -selection primary | xclip -in -selection clipboard
    xdg-open https://www.deepl.com/translator
    xdotool search --name ".*DeepL.*" --classname "Navigator" windowactivate --sync
    sleep 1.5
    xdotool key --clearmodifiers ctrl+v
fi
