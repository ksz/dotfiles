#!/bin/bash

myprimarydns=192.168.*.*
myfallbackdns=1.1.1.1
# myfallbackdns=9.9.9.9

function_systemd () {
    if grep -q "$myprimarydns" /run/systemd/resolve/resolv.conf; then
        sudo resolvectl dns $(nmcli -t -f DEVICE c show --active | head -n 1) "$myfallbackdns"
    else
        sudo resolvectl dns $(nmcli -t -f DEVICE c show --active | head -n 1) "$myprimarydns"
    fi
    cat /run/systemd/resolve/resolv.conf | grep ^nameserver | sed "s/nameserver/DNS has changed to:/"
}

function_nosystemd () {
    if grep -q "$myprimarydns" /etc/resolv.conf; then
        sudo sed -i "s/$myprimarydns/$myfallbackdns/" /etc/resolv.conf
    else
        sudo sed -i "s/$myfallbackdns/$myprimarydns/" /etc/resolv.conf
    fi
    cat /etc/resolv.conf | grep ^nameserver | sed "s/nameserver/DNS has changed to:/"
}

if pidof -q systemd; then
    function_systemd
else
    function_nosystemd
fi
