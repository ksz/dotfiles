#!/bin/bash

sudo dnf upgrade --refresh
reboot

sudo dnf install dnf-plugin-system-upgrade
sudo dnf system-upgrade download --releasever=41
# sudo dnf system-upgrade download --allowerasing --releasever=41
sudo dnf system-upgrade reboot


### Optional post-upgrade tasks
# Remove unwanted apps
sudo dnf remove gnome-disk-utility gnome-abrt setroubleshoot im-chooser system-config-language
# Update system configuration files
sudo dnf install rpmconf
sudo rpmconf -a
# Clean-up retired packages
sudo dnf install remove-retired-packages
remove-retired-packages
# Clean-up old packages
sudo dnf repoquery --unsatisfied
sudo dnf repoquery --duplicates
sudo dnf remove --duplicates
sudo dnf list extras
sudo dnf remove $(sudo dnf repoquery --extras --exclude=kernel,kernel-\*)
sudo dnf autoremove
# Clean-up old symlinks
sudo dnf install symlinks
sudo symlinks -r /usr | grep dangling
sudo symlinks -r -d /usr
# Update rescue kernel
sudo rm /boot/*rescue*
sudo kernel-install add "$(uname -r)" "/lib/modules/$(uname -r)/vmlinuz"
# Resolve dependency issues
sudo dnf distro-sync
# sudo dnf distro-sync --allowerasing


### BIOS systems
# Update GRUB bootloader
[ -d /sys/firmware/efi ] && echo UEFI || echo BIOS
lsblk
sudo mount | grep "/boot "
sudo grub2-install /dev/sda
sudo grub2-mkconfig -o /boot/grub2/grub.cfg


# https://docs.fedoraproject.org/en-US/quick-docs/upgrading-fedora-offline/
