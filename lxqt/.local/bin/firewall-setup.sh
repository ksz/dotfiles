#!/bin/bash

mydevice=$(nmcli -t -f DEVICE c show --active | head -n 1)
myname=$(nmcli -t -f NAME c show --active | head -n 1)

sudo firewall-cmd --zone=home --change-interface=$mydevice --permanent
sudo nmcli connection modify "$myname" connection.zone home
sudo firewall-cmd --reload

if whereis kdeconnect-app | grep -q kdeconnect-app; then
    sudo firewall-cmd --zone=home --add-service=kdeconnect --permanent
    sudo firewall-cmd --reload
fi

echo
firewall-cmd --get-active-zones
echo
nmcli connection show
echo
