#!/bin/bash

sleep 5
notify-send -t 3600000 -u normal "first-run.sh" "Run scripts:\n- firewall-setup.sh\n- firefox-setup.sh"
systemctl --user disable first-run.service
