#!/bin/bash

myfile="$HOME/.local/share/hotkeys.txt"

function_yad () {
    mywindow=$(wmctrl -l -x | grep -E "yad.Yad .* Hotkeys$" | awk '{ print $1 }')
    if [ -z "$mywindow" ]; then
        yad --text-info < $myfile --title=Hotkeys --window-icon=keyboard --width=455 --height=735 \
            --no-buttons --center --on-top --undecorated --fontname='Hack 9' # --close-on-unfocus
    else
        wmctrl -i -c "$mywindow"
    fi
}

function_zenity () {
    export GTK_THEME=Adwaita
    mywindow=$(xdotool search --onlyvisible --class "zenity"; xdotool search --name "Hotkeys" | sort | uniq -d)
    if [ -z "$mywindow" ]; then
        zenity --text-info --filename="$myfile" --title="Hotkeys" \
               --width=455 --height=735 --font="Hack 9"
    else
        xdotool windowkill "$mywindow"
    fi
}

if which yad; then
    function_yad
else
    function_zenity
fi
