#!/bin/bash

pkill -USR1 ^redshift$ && \
notify-send -u normal "nightlight-toggle.sh" "Screen color temperature has been changed."
