#!/bin/bash

myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/$1/helpers"

if [ "$1" = "arch" ]; then
    cp $myhelp/.{bash_profile,bashrc,xprofile} ~/.
elif [ "$1" = "debian" ]; then
    cp -f $myhelp/.{bash_aliases,xsessionrc} ~/.
    sed -i "s/_Light/_Snow/;s/22/16/" ~/.{config/gtk-3.0/settings.ini,config/lxqt/session.conf,icons/default/index.theme,gtkrc-2.0,Xdefaults,Xresources}
else
    cp -f $myhelp/.bashrc ~/.
fi

if [ "$2" = "artix" ]; then
    sed -i "1i pipewire &\npipewire-pulse &\nwireplumber &" ~/.xprofile
    ### xcursor-breeze fix
    if test -d /usr/share/icons/Breeze_Snow/Breeze_Snow/cursors; then
        sudo mv /usr/share/icons/Breeze_Snow /usr/share/icons/Breeze_Snow.bak
        sudo ln -sf /usr/share/icons/Breeze_Snow.bak/Breeze_Snow /usr/share/icons/Breeze_Snow
    fi
fi

if [ "$2" = "debian" ]; then
    ### remmina tray icon fix
    for i in 16x16 22x22 24x24; do
        mkdir -p ~/.icons/hicolor/$i/status
        ln -sf /usr/share/icons/Papirus-Light/$i/panel/remmina-panel.svg ~/.icons/hicolor/$i/status/org.remmina.Remmina-status.svg
    done
fi

xdg-user-dirs-update
ln -sf $myhelp/$2-wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
ln -sf $myhelp/$2-wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg
find ~/.local/bin -type f -exec chmod +x "{}" \;
if pidof systemd; then
    systemctl --user enable conf-set.service
    systemctl --user enable first-run.service
fi

if whoami | grep -q "$myuser"; then
    echo "Current user: $myuser"
    sudo mkdir -p /root/.config/{gtk-3.0,lxqt}
    sudo cp -rf ~/.config/{htop,mc} /root/.config
    sudo cp -f ~/.config/gtk-3.0/settings.ini /root/.config/gtk-3.0
    sudo cp -f ~/.config/lxqt/{lxqt.conf,lxqt-config-appearance.conf} /root/.config/lxqt
    sudo sed -i "s/modarin256/modarin256root/" /root/.config/mc/ini
    sed -i "936s/<\!-- //;945s/ -->//" ~/.config/openbox/rc.xml
#     sudo sh -c "echo -e '\nexport EDITOR=/usr/bin/nano' >> /etc/environment"
    if grep -q VIRT /etc/hostname; then
        sudo cp -f $myhelp/00-keyboard.conf /etc/X11/xorg.conf.d
    fi
    if whereis -b k3b | grep -q "k3b: /"; then
        sudo usermod -aG cdrom "$myuser"
        sudo chown root:cdrom /usr/bin/{cdrdao,growisofs,wodim}
        sudo chmod 4710 /usr/bin/{cdrdao,wodim}
        sudo chmod 0750 /usr/bin/growisofs
    fi
else
    echo "Current user: $(whoami)"
    sed -i "/Hidden=/s/false/true/" ~/.config/autostart/{conky,copyq}.desktop
    sed -i "/iconSize=/s/16/22/;/panelSize=/s/28/32/" ~/.config/lxqt/panel.conf
    sed -i "/lock_screen_before_power_actions=/s/true/false/" ~/.config/lxqt/session.conf
fi

if grep -q VIRT /etc/hostname; then
    sed -i "/Hidden=/s/false/true/" ~/.config/autostart/{picom,redshift}.desktop
    sed -i "/Watcher=true$/s/true/false/g" ~/.config/lxqt/lxqt-powermanagement.conf
    sed -i "/^dpmsEnabled:/s/True/False/;/^mode:/s/blank/off/" ~/.xscreensaver
fi

if env | grep -q LANG=en_US.UTF-8; then
    sed -i "s/<PICTURES>/Pictures/" ~/.{config,local/src/dotfiles/lxqt/.config}/pcmanfm-qt/lxqt/settings.conf
    echo '#!/bin/bash' | tee $(xdg-user-dir TEMPLATES)/Script.sh
else
    sed -i "s/\&clipboard/\&schowek/g" ~/.config/copyq/copyq.conf
    sed -i "s/<PICTURES>/Obrazy/" ~/.{config,local/src/dotfiles/lxqt/.config}/pcmanfm-qt/lxqt/settings.conf
    echo '#!/bin/bash' | tee $(xdg-user-dir TEMPLATES)/Skrypt.sh
fi

echo

if whereis -b aria2c | grep -q "aria2c: /"; then
    echo "+ aria2 is installed."
else
    rm -rf ~/.config/aria2 && \
    echo "- aria2 is not installed."
fi

if whereis -b avahi-discover | grep -q "avahi-discover: /"; then
    echo "+ Avahi Zeroconf Browser is installed."
else
    rm -f ~/.local/share/applications/avahi-discover.desktop && \
    echo "- Avahi Zeroconf Browser is not installed."
fi

if whereis -b bssh | grep -q "bssh: /"; then
    echo "+ Avahi SSH Server Browser is installed."
else
    rm -f ~/.local/share/applications/bssh.desktop && \
    echo "- Avahi SSH Server Browser is not installed."
fi

if whereis -b bvnc | grep -q "bvnc: /"; then
    echo "+ Avahi VNC Server Browser is installed."
else
    rm -f ~/.local/share/applications/bvnc.desktop && \
    echo "- Avahi VNC Server Browser is not installed."
fi

if whereis -b conky | grep -q "conky: /"; then
    echo "+ Conky is installed."
else
    rm -f ~/.config/autostart/conky.desktop && \
    rm -rf ~/.config/conky && \
    rm -f ~/.local/share/applications/conky.desktop && \
    echo "- Conky is not installed."
fi

if whereis -b copyq | grep -q "copyq: /"; then
    echo "+ CopyQ is installed."
else
    rm -f ~/.config/autostart/copyq.desktop && \
    rm -rf ~/.config/copyq && \
    echo "- CopyQ is not installed."
fi

# if whereis -b deadbeef | grep -q "deadbeef: /"; then
#     echo "+ DeaDBeeF is installed."
# else
#     rm -rf ~/.config/deadbeef && \
#     echo "- DeaDBeeF is not installed."
# fi

if test -f /usr/share/applications/firefox-esr.desktop; then
    sed -i "s/org.mozilla.firefox/firefox-esr/g" ~/.config/lxqt-mimeapps.list && \
    sed -i "s/org.mozilla.firefox/firefox-esr/" ~/.config/lxqt/panel.conf && \
    echo "+ Firefox ESR is installed."
else
    echo "- Firefox ESR is not installed."
fi

if whereis -b gnome-keyring | grep -q "gnome-keyring: /"; then
    echo "+ Gnome Keyring is installed."
else
    rm -f ~/.config/autostart/gnome-keyring-*.desktop && \
    echo "- Gnome Keyring is not installed."
fi

if whereis -b htop | grep -q "htop: /"; then
    echo "+ htop is installed."
else
    rm -rf ~/.config/htop && \
    rm -f ~/.local/share/applications/htop.desktop && \
    echo "- htop is not installed."
fi

if whereis -b keepassxc | grep -q "keepassxc: /"; then
    echo "+ KeePassXC is installed."
else
    rm -rf ~/.config/keepassxc && \
    echo "- KeePassXC is not installed."
fi

if whereis -b mc | grep -q "mc: /"; then
    echo "+ Midnight Commander is installed."
else
    rm -rf ~/.config/mc && \
    rm -f ~/.local/share/applications/mc*.desktop && \
    echo "- Midnight Commander is not installed."
fi

if whereis -b picom | grep -q "picom: /"; then
    echo "+ picom is installed."
else
    rm -f ~/.config/autostart/picom.desktop && \
    rm -rf ~/.config/picom && \
    echo "- picom is not installed."
fi

if whereis -b qv4l2 | grep -q "qv4l2: /"; then
    echo "+ Qt V4L2 test utility is installed."
else
    rm -f ~/.local/share/applications/qv4l2.desktop && \
    echo "- Qt V4L2 test utility is not installed."
fi

if whereis -b qvidcap | grep -q "qvidcap: /"; then
    echo "+ Qt V4L2 video capture utility is installed."
else
    rm -f ~/.local/share/applications/qvidcap.desktop && \
    echo "- Qt V4L2 video capture utility is not installed."
fi

if whereis -b redshift | grep -q "redshift: /"; then
    echo "+ Redshift is installed."
else
    rm -f ~/.config/autostart/redshift.desktop && \
    rm -rf ~/.config/redshift && \
    echo "- Redshift is not installed."
fi

# if whereis -b seahorse | grep -q "seahorse: /"; then
#     echo "+ Seahorse is installed."
# else
#     rm -f ~/.local/share/applications/org.gnome.seahorse.Application.desktop && \
#     echo "- Seahorse is not installed."
# fi

if whereis -b screengrab | grep -q "screengrab: /"; then
    echo "+ ScreenGrab is installed."
else
    rm -rf ~/.config/screengrab && \
    echo "- ScreenGrab is not installed."
fi

if whereis -b st | grep -q "st: /"; then
    echo "+ st is installed."
else
    rm -f ~/.local/share/applications/st.desktop && \
    echo "- st is not installed."
fi

if whereis -b system-config-printer | grep -q "system-config-printer: /usr/bin"; then
    echo "+ system-config-printer is installed."
else
    rm -f ~/.config/autostart/print-applet.desktop && \
    echo "- system-config-printer is not installed."
fi

if whereis -b vlc | grep -q "vlc: /"; then
    echo "+ VLC is installed."
else
    rm -rf ~/.config/vlc && \
    echo "- VLC is not installed."
fi

if whereis -b xscreensaver | grep -q "xscreensaver: /"; then
    echo "+ XScreenSaver is installed."
else
    rm -f ~/.local/share/applications/xscreensaver.desktop && \
    rm -f ~/.xscreensaver && \
    echo "- XScreenSaver is not installed."
fi

if whereis -b xterm | grep -q "xterm: /"; then
    echo "+ xterm is installed."
else
    rm -f ~/.local/share/applications/debian-*xterm.desktop && \
    echo "- xterm is not installed."
fi



# if whereis -b anydesk | grep -q "anydesk: /"; then
#     echo "+ AnyDesk is installed."
# else
#     rm -f ~/.local/share/applications/anydesk.desktop && \
#     echo "- AnyDesk is not installed."
# fi

# if whereis -b gcdmaster | grep -q "gcdmaster: /"; then
#     echo "+ Gnome CD Master is installed."
# else
#     rm -f ~/.local/share/applications/gcdmaster.desktop && \
#     echo "- Gnome CD Master is not installed."
# fi

# if whereis -b gpxsee | grep -q "gpxsee: /"; then
#     echo "+ GPXSee is installed."
# else
#     rm -f ~/.local/share/applications/gpxsee.desktop && \
#     echo "- GPXSee is not installed."
# fi

# if whereis -b gwenview | grep -q "gwenview: /"; then
#     echo "+ Gwenview is installed."
# else
#     rm -f ~/.config/gwenviewrc && \
#     echo "- Gwenview is not installed."
# fi

# if whereis -b kdeconnect-app | grep -q "kdeconnect-app: /"; then
#     echo "+ KDE Connect is installed."
# else
#     rm -f ~/.config/autostart/org.kde.kdeconnect.daemon.desktop && \
#     rm -f ~/.local/share/applications/org.kde.kdeconnect.*.desktop && \
#     echo "- KDE Connect is not installed."
# fi

# if whereis -b mpv | grep -q "mpv: /"; then
#     echo "+ mpv is installed."
# else
#     rm -rf ~/.config/mpv && \
#     echo "- mpv is not installed."
# fi

# if whereis -b nsxiv | grep -q "nsxiv: /"; then
#     echo "+ nsxiv is installed."
# else
#     sed -i "s/nsxiv/lximage-qt/g" ~/.config/lxqt-mimeapps.list && \
#     rm -rf ~/.config/nsxiv && \
#     rm -f ~/.local/bin/nsxiv-rifle && \
#     rm -f ~/.local/share/applications/{lximage-qt,nsxiv}.desktop && \
#     echo "- nsxiv is not installed."
# fi

# if whereis -b qdirstat | grep -q "qdirstat: /"; then
#     echo "+ QDirStat is installed."
# else
#     rm -f ~/.local/share/applications/qdirstat.desktop && \
#     echo "- QDirStat is not installed."
# fi

# if whereis -b qlipper | grep -q "qlipper: /"; then
#     echo "+ Qlipper is installed."
# else
#     rm -f ~/.config/autostart/lxqt-qlipper-autostart.desktop && \
#     rm -f ~/.local/share/applications/qlipper.desktop && \
#     echo "- Qlipper is not installed."
# fi

# if whereis -b qpdfview | grep -q "qpdfview: /usr/bin"; then
#     sed -i "s/org.pwmt.zathura/qpdfview/g" ~/.config/lxqt-mimeapps.list && \
#     echo "+ qpdfview is installed."
# else
#     echo "- qpdfview is not installed."
# fi

# if whereis -b qpdfview-qt6 | grep -q "qpdfview-qt6: /"; then
#     sed -i "s/org.pwmt.zathura/qpdfview-qt6/g" ~/.config/lxqt-mimeapps.list && \
#     echo "+ qpdfview is installed."
# else
#     rm -f ~/.local/share/applications/qpdfview-qt6.desktop && \
#     echo "- qpdfview is not installed."
# fi

# if whereis -b rofi | grep -q "rofi: /"; then
#     echo "+ Rofi is installed."
# else
#     rm -f ~/.local/share/applications/rofi*.desktop && \
#     echo "- Rofi is not installed."
# fi

# if whereis -b rustdesk | grep -q "rustdesk: /"; then
#     echo "+ RustDesk is installed."
# else
#     rm -f ~/.local/share/applications/rustdesk.desktop && \
#     echo "- RustDesk is not installed."
# fi

# if whereis -b sqlitebrowser | grep -q "sqlitebrowser: /"; then
#     echo "+ DB Browser for SQLite is installed."
# else
#     rm -f ~/.local/share/applications/sqlitebrowser.desktop && \
#     echo "- DB Browser for SQLite is not installed."
# fi

# if whereis -b zathura | grep -q "zathura: /"; then
#     echo "+ zathura is installed."
# else
#     rm -f ~/.local/share/applications/org.pwmt.zathura*.desktop && \
#     echo "- zathura is not installed."
# fi
