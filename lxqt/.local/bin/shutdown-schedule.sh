#!/bin/bash

read -p "Enter timeout in minutes: " mytimeout

if [ -z "$mytimeout" ] || [ -z "${mytimeout##*[!0-9]*}" ] || [ "$mytimeout" -lt 1 ]; then
    echo "Incorrect timeout value. Try again."
    exit
fi

for i in $(eval echo "{$mytimeout..1}"); do
    echo "System will shut down in ${i} minute(s)..."
    for i in $(seq 59 -1 0); do echo -ne "\r$i "; sleep 1; done
    echo -ne "\033[0K\r"
    clear
done

echo "The computer is about to shut down."
sleep 5
systemctl poweroff



### minutes to seconds
# mytimeout=$(($mytimeout*60))
