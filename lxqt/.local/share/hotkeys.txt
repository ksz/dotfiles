
  Shift+Meta+D ................ Show desktop
  Shift+Meta+L ................ Lock screen
  Shift+Meta+Del .............. Leave dialog
  Shift+Meta+R ................ App launcher
  Shift+Meta+/ ................ Hotkeys help

  Shift+Meta+F ................ File manager
  Shift+Meta+I ................ Web browser
  Shift+Meta+Return ........... Terminal
  Shift+Meta+E ................ Text editor
  Shift+Meta+N ................ Night light
  Shift+Meta+' ................ Clipboard
  Ctrl+Alt+K .................. Password manager
  Meta+Space .................. DeepL translate

  Shift+Meta+{=-} ............. In/Decrease volume
  Shift+Meta+0 ................ Un/Mute volume

  Print ....................... Screenshot (select)
  Meta+Print .................. Screenshot (active)
  Shift+Meta+Print ............ Screenshot (full)

  Alt+F4 ...................... Close window
  Alt+Esc ..................... Unfocus window
  Alt+Space ................... Show menu

  [Shift]+Alt+Tab ............. Previous/Next window
  Shift+Meta+{LRUD} ........... Switch window
  Meta+{LRUD} ................. Snap window to edge
  Shift+Meta+{,.} ............. Move window to monitor {1,2}
  [Ctrl+Shift]+Meta+{=-} ...... In/Decrease window size
  Ctrl+Shift+Meta+{LRUD} ...... Move window

  Meta+A ...................... Un/Maximize window
  Meta+I ...................... Minimize window
  Meta+H ...................... Horizontal tiling
  Meta+V ...................... Vertical tiling
  Meta+C ...................... Center tiling windows
  Shift+Meta+C ................ Center current window

  Meta+{1-9} .................. Activate task {1-9}
  Meta+0 ...................... Activate task {10}

  Ctrl+{F1-F12} ............... Go to desktop {1-12}
  Ctrl+Alt+{LRUD} ............. Go to desktop
  Shift+Alt+{LRUD} ............ Send to desktop
  Meta+{F1-F4} ................ Go to desktop {1-4}

  Alt+F1 ...................... Main menu
  Alt+F2 ...................... Runner dialog
