#!/bin/bash

myrun=0
myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)

if uname -a | grep -iq debian; then mydistro=debian; else mydistro=arch; fi
myhelp="$HOME/.local/src/setup/$mydistro/helpers"

dconf load / < $myhelp/backup-dconf
gsettings set org.mate.background picture-filename "$(xdg-user-dir PICTURES)/wallpaper.jpg"

if whoami | grep -q "$myuser"; then
    echo "Current user: $myuser"
else
    echo "Current user: $(whoami)"
    gsettings set org.mate.screensaver lock-enabled false
fi

if grep -q VIRT /etc/hostname; then
    gsettings set org.mate.power-manager sleep-computer-ac 0
    gsettings set org.mate.power-manager sleep-display-ac 0
    gsettings set org.mate.screensaver idle-activation-enabled false
    gsettings set org.mate.screensaver lock-enabled false
fi

if systemctl --user is-enabled conf-set.service; then
    myrun=$((myrun + 1))
    sed -i "3 s/^.*$/myrun=$myrun/" $HOME/.local/bin/conf-set.sh
    if [ "$myrun" -gt "2" ]; then
        systemctl --user disable conf-set.service && \
        echo "Unit conf-set.service disabled." && \
        sleep 5 && \
        notify-send -t 10000 -u normal "conf-set.sh" "Unit conf-set.service disabled."
    fi
fi
