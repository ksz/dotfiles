#!/bin/bash

myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/$1/helpers"

if [ "$1" = "arch" ]; then
    cp $myhelp/.{bash_profile,bashrc,xprofile} ~/.
elif [ "$1" = "debian" ]; then
    cp $myhelp/.{bash_aliases,xsessionrc} ~/.
else
    cp $myhelp/.{bash_profile,bashrc} ~/.
fi

xdg-user-dirs-update
ln -sf $myhelp/$2-wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
ln -sf $myhelp/$2-wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg
dconf load / < $myhelp/backup-dconf
gsettings set org.mate.background picture-filename "$(xdg-user-dir PICTURES)/wallpaper.jpg"
chmod +x ~/.local/bin/*.sh

if whoami | grep -q "$myuser"; then
    echo "Current user: $myuser"
    sudo mkdir /root/.config
    sudo cp -r ~/.config/{htop,mc} /root/.config
    sudo sed -i "/Float-into-MATE.png/s/^# //" /etc/lightdm/lightdm-gtk-greeter.conf
    # if which gtk3-nocsd | grep -q gtk3-nocsd; then
        # sudo apt-get -y purge gtk3-nocsd
        # sudo apt-get -y autoremove
    # fi
    if grep -q VIRT /etc/hostname; then
        # sudo cp /etc/default/keyboard /etc/default/keyboard.bak
        # sudo sed -i '/^XKBLAYOUT=/s/".*"/"pl,us"/' /etc/default/keyboard
        # sudo sed -i '/XKBOPTIONS=""/s/""/"caps:swapescape"/' /etc/default/keyboard
        sudo cp $myhelp/00-keyboard.conf /etc/X11/xorg.conf.d
    fi
else
    echo "Current user: $(whoami)"
    gsettings set org.mate.screensaver lock-enabled false && \
    echo "Lock screen disabled." && \
    notify-send -t 3600000 -u normal "post-install.sh" "Lock screen disabled."
fi

if grep -q VIRT /etc/hostname; then
    # sed -i "/set-sink-mute/s/false/true/;/set-sink-volume/s/^/# /" ~/.config/pulse/default.pa && \
    # echo "Sound muted." && \
    # notify-send -t 3600000 -u normal "post-install.sh" "Sound muted."
    gsettings set org.mate.power-manager sleep-computer-ac 0 && \
    gsettings set org.mate.power-manager sleep-display-ac 0 && \
    gsettings set org.mate.screensaver idle-activation-enabled false && \
    gsettings set org.mate.screensaver lock-enabled false && \
    echo "Automatic sleep disabled." && \
    notify-send -t 3600000 -u normal "post-install.sh" "Automatic sleep disabled."
fi

if env | grep -q LANG=en_US.UTF-8; then
    echo '#!/bin/bash' | tee $(xdg-user-dir TEMPLATES)/Script.sh && \
    echo "The main system language is English." && \
    notify-send -t 3600000 -u normal "post-install.sh" "The main system language is English."
else
    echo '#!/bin/bash' | tee $(xdg-user-dir TEMPLATES)/Skrypt.sh && \
    echo "The main system language is Polish." && \
    notify-send -t 3600000 -u normal "post-install.sh" "The main system language is Polish."
fi

### Remmina tray icon fix
# if dconf read /org/mate/desktop/interface/icon-theme | grep -q Papirus; then
    # for i in 16x16 22x22 24x24; do
        # mkdir -p ~/.icons/hicolor/$i/status
        # ln -s /usr/share/icons/Papirus/$i/panel/remmina-panel.svg ~/.icons/hicolor/$i/status/org.remmina.Remmina-status.svg
    # done
# fi
