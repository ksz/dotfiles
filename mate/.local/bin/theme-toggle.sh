#!/bin/bash

if uname -a | grep -iq debian; then mycursorlight=Breeze_Snow; else mycursorlight=Breeze_Light; fi

if dconf read /org/mate/desktop/interface/gtk-theme | grep -q Arc-Dark; then
    ### Remmina tray icon fix
    # for i in 16x16 22x22 24x24; do
        # ln -sf /usr/share/icons/Papirus-Light/$i/panel/remmina-panel.svg ~/.icons/hicolor/$i/status/org.remmina.Remmina-status.svg
    # done
    dconf write /org/mate/desktop/interface/gtk-theme "'Arc-Lighter'"
    dconf write /org/mate/desktop/interface/icon-theme "'Papirus-Light'"
    dconf write /org/mate/desktop/peripherals/mouse/cursor-theme "'Breeze'"
    dconf write /org/mate/marco/general/theme "'Arc-Lighter'"
    dconf write /org/mate/pluma/color-scheme "'kate'"
else
    ### Remmina tray icon fix
    # for i in 16x16 22x22 24x24; do
        # ln -sf /usr/share/icons/Papirus-Dark/$i/panel/remmina-panel.svg ~/.icons/hicolor/$i/status/org.remmina.Remmina-status.svg
    # done
    dconf write /org/mate/desktop/interface/gtk-theme "'Arc-Dark'"
    dconf write /org/mate/desktop/interface/icon-theme "'Papirus-Dark'"
    dconf write /org/mate/desktop/peripherals/mouse/cursor-theme "'$mycursorlight'"
    dconf write /org/mate/marco/general/theme "'Arc-Dark'"
    dconf write /org/mate/pluma/color-scheme "'oblivion'"
fi
