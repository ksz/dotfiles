# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi
unset rc

alias ping='ping -c 5'
alias sudo='sudo '
alias wget='wget -c'
alias bc='bc -lq'
alias df='df -h'
alias du='du -ah'
alias ht='htop -t'
alias ka='killall'
alias ll='ls -la'
alias mc='mc -x'
alias nn='nano -l'
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -Iv'
alias md='mkdir -pv'
alias rd='rmdir -v'
# alias sdu='sudo dnf upgrade --refresh'
alias sdu='sudo dnf upgrade'
alias sda='sudo dnf autoremove'
alias sdi='sudo dnf install'
alias sdr='sudo dnf remove'
alias sdl='dnf list --installed | grep'

alias dcu='cd ~/docker && docker compose up -d && cd ~'
alias dcd='cd ~/docker && docker compose down && cd ~'
alias dcs='cd ~/docker && docker compose stop && cd ~'
alias dba='docker-backup.sh'
alias dre='docker-restore.sh'
alias dup='docker-update.sh'
alias drc='docker rm -f $(docker ps -aq)'
alias drv='docker volume rm $(docker volume ls -q)'

alias f2b='sudo fail2ban-client status --all'

PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

sleep 0.2
if [ "$(whoami)" = "opc" ]; then sudo hostnamectl set-hostname VPS; fi
