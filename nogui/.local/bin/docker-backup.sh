#!/bin/bash

volumes=(
nginx_data
mariadb-aria_data
portainer_data
uptime-kuma_data
)

workdir=$HOME/docker
today=$(date +%Y%m%d)

if ! test -d $workdir/volumes-$today; then mkdir $workdir/volumes-$today; fi

cd $workdir && docker compose stop

for volume in ${volumes[@]}; do
    cd $workdir/volumes-$today
    docker run --rm \
    --mount source=docker_$volume,target=/var/lib/docker/volumes/docker_$volume/_data \
    -v $(pwd):/backup \
    busybox:latest \
    tar -czvf /backup/$volume.tar.gz /var/lib/docker/volumes/docker_$volume/_data
done

cd $workdir && docker compose up -d
