#!/bin/bash

if [ "$(docker ps -q -f name=nextcloud)" ]; then
    docker exec -u www-data nextcloud php -f /var/www/html/cron.php
fi
