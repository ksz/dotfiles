#!/bin/bash

excluded=(
nextcloud_data
mariadb_data
redis_data
)

excluded=$(printf "%s|" "${excluded[@]}")
excluded=${excluded%|}
workdir=$HOME/docker

cd $workdir && docker compose down

if [ -z "$excluded" ]; then
    docker volume rm $(docker volume ls -q)
else
    docker volume rm $(docker volume ls -q | grep -v -E "$excluded")
fi

cd $workdir && docker compose up -d
cd $workdir && docker compose stop

for file in $workdir/volumes/*.tar.gz; do
    volume=$(basename $file .tar.gz)
    cd $workdir/volumes
    docker run --rm \
    --mount source=docker_$volume,target=/var/lib/docker/volumes/docker_$volume/_data \
    -v $(pwd):/backup \
    busybox:latest \
    tar -xzvf /backup/$volume.tar.gz -C /
done

cd $workdir && docker compose up -d
