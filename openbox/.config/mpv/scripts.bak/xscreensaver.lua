local utils = require 'mp.utils'

local function heartbeat()
    if mp.get_property("vo-configured") == "yes" and mp.get_property("pause") == "no" then
        utils.subprocess({args={"xscreensaver-command", "-deactivate"}})
    end
end

mp.add_periodic_timer(30, heartbeat)


-- this script periodically deactivates xscreensaver when mpv window is visible and playback is not paused
-- if you don't want to deactivate xscreensaver while using mpv, run this commands in terminal:
-- "mpv --no-video *"
-- "mpv --no-audio-display *"
