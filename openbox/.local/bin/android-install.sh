#!/bin/bash

mydistro=$(grep -o -P '(?<=^ID=).*(?=$)' /etc/os-release)

apps-install.sh --eget
apps-install.sh --fdroidcl

if [ "$mydistro" = "arch" ]; then
    sudo pacman -S --noconfirm --needed android-tools android-udev scrcpy
    exit
elif [ "$mydistro" = "debian" ]; then
    . /etc/os-release
    sudo apt-get -y install -t $VERSION_CODENAME-backports adb fastboot # scrcpy
elif [ "$mydistro" = "fedora" ]; then
    sudo dnf -y install android-tools
    sudo dnf -y copr enable zeno/scrcpy
    sudo dnf -y install scrcpy
fi

git clone https://github.com/M0Rf30/android-udev-rules.git

sudo cp -f android-udev-rules/51-android.rules /etc/udev/rules.d
sudo chmod a+r /etc/udev/rules.d/51-android.rules

sudo cp -f android-udev-rules/android-udev.conf /usr/lib/sysusers.d
sudo systemd-sysusers
sudo gpasswd -a $(whoami) adbusers

sudo udevadm control --reload-rules
sudo systemctl restart systemd-udevd.service

if pgrep adb >/dev/null; then adb kill-server; fi

rm -rf android-udev-rules


### In case of problems with a distro package:
# wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip && \
# unzip platform-tools*.zip && \
# rm -f platform-tools*.zip
# cd platform-tools
# ./adb --version
# ./fastboot --version
