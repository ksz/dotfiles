#!/bin/bash

myaudioicon=audio-speakers

function_lineout () {
    pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 2, save: true }'
    pw-metadata 0 default.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.analog-stereo.4" }'
    pw-cli s 40 Route '{ index: 2, device: 7 }'
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q MUTED && wpctl set-mute @DEFAULT_AUDIO_SINK@ 0
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q 1.00 || wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
    notify-send -t 10000 -u normal -i $myaudioicon "audio-toggle.sh" "Audio output: Line Out"
}

function_displayport1 () {
    pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 7, save: true }'
    pw-metadata 0 default.configured.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1" }'
    pw-cli s 40 Route '{ index: 6, device: 11 }'
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q MUTED && wpctl set-mute @DEFAULT_AUDIO_SINK@ 0
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q 1.00 || wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
    notify-send -t 10000 -u normal -i $myaudioicon "audio-toggle.sh" "Audio output: DisplayPort 1"
}

function_displayport2 () {
    pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 8, save: false }'
    pw-metadata 0 default.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra2.2" }'
    pw-cli s 40 Route '{ index: 7, device: 12 }'
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q MUTED && wpctl set-mute @DEFAULT_AUDIO_SINK@ 0
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q 1.00 || wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
    notify-send -t 10000 -u normal -i $myaudioicon "audio-toggle.sh" "Audio output: DisplayPort 2"
}

function_hdmi () {
    pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 4, save: false }'
    pw-metadata 0 default.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.hdmi-stereo" }'
    pw-cli s 40 Route '{ index: 5, device: 8 }'
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q MUTED && wpctl set-mute @DEFAULT_AUDIO_SINK@ 0
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q 1.00 || wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
    notify-send -t 10000 -u normal -i $myaudioicon "audio-toggle.sh" "Audio output: HDMI"
}

function_headphones () {
    pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 2, save: false }'
    pw-metadata 0 default.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.analog-stereo.4" }'
    pw-cli s 40 Route '{ index: 4, device: 7 }'
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q MUTED && wpctl set-mute @DEFAULT_AUDIO_SINK@ 0
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q 1.00 || wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
    notify-send -t 10000 -u normal -i $myaudioicon "audio-toggle.sh" "Audio output: Headphones"
}

function_speakers () {
    pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 2, save: false }'
    pw-metadata 0 default.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.analog-stereo.4" }'
    pw-cli s 40 Route '{ index: 3, device: 7 }'
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q MUTED && wpctl set-mute @DEFAULT_AUDIO_SINK@ 0
    wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q 1.00 || wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
    notify-send -t 10000 -u normal -i $myaudioicon "audio-toggle.sh" "Audio output: Speakers"
}

function_menu () {
    choices="lineout\ndisplayport1\ndisplayport2\nhdmi\nheadphones\nspeakers"
    chosen=$(echo -e "$choices" | rofi -dmenu -i -l 6 -theme-str 'window {width: 8%;}' -p audio)
    case "$chosen" in
        lineout     ) function_lineout ;;
        displayport1) function_displayport1 ;;
        displayport2) function_displayport2 ;;
        hdmi        ) function_hdmi ;;
        headphones  ) function_headphones ;;
        speakers    ) function_speakers ;;
    esac
}

case "$1" in
    -l|--lineout     ) function_lineout ;;
    -d|--displayport1) function_displayport1 ;;
    -t|--displayport2) function_displayport2 ;;
    -h|--hdmi        ) function_hdmi ;;
    -p|--headphones  ) function_headphones ;;
    -s|--speakers    ) function_speakers ;;
    -m|--menu        ) function_menu ;;
esac



# https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Migrate-PulseAudio#set-card-profile
# https://wiki.archlinux.org/title/PipeWire
# https://wiki.archlinux.org/title/WirePlumber

# pw-cli ls Device

# pw-cli e alsa_card.pci-0000_00_1f.3 EnumProfile
# pw-cli s alsa_card.pci-0000_00_1f.3 Profile '{ index: 7, save: true }'

# pw-cli ls Node
# pw-metadata 0 default.audio.sink '{ "name": "alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1" }'

# pw-cli i alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1
# pw-cli e alsa_card.pci-0000_00_1f.3 EnumRoute
# pw-cli s 40 Route '{ index: 6, device: 11 }'

# ---

# OP7050 (Eizo <---> Samsung)
# if pactl get-default-sink | grep -q "alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1"; then
#     pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo
#     pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo
#     pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo hdmi-output-0
#     pactl set-sink-mute @DEFAULT_SINK@ false
#     pactl set-sink-volume @DEFAULT_SINK@ 100%
#     notify-send -t 5000 -u normal "audio-toggle.sh" "Audio device:\n SAMSUNG"
# else
#     pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo-extra1
#     pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1
#     pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1 hdmi-output-1
#     pactl set-sink-mute @DEFAULT_SINK@ false
#     pactl set-sink-volume @DEFAULT_SINK@ 100%
#     notify-send -t 5000 -u normal "audio-toggle.sh" "Audio device:\n EIZO"
# fi



# pacmd list-cards
# pacmd list-sinks

# pactl set-sink-volume @DEFAULT_SINK@ 100%
# pactl set-sink-volume @DEFAULT_SINK@ +10%
# pactl set-sink-volume @DEFAULT_SINK@ -10%
# pactl set-sink-mute @DEFAULT_SINK@ toggle

# wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
# wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%+
# wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%-
# wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
