#!/bin/bash

myrun=0
myinfoicon=dialog-information

gsettings set org.gtk.Settings.FileChooser date-format 'with-time'
gsettings set org.gtk.Settings.FileChooser show-hidden true
gsettings set org.gtk.Settings.FileChooser sort-directories-first true
gsettings set org.gtk.gtk4.Settings.FileChooser date-format 'with-time'
gsettings set org.gtk.gtk4.Settings.FileChooser show-hidden true
gsettings set org.gtk.gtk4.Settings.FileChooser sort-directories-first true

if which meld | grep -q meld; then
    gsettings set org.gnome.meld custom-font 'hack 9'
    gsettings set org.gnome.meld show-line-numbers true
    gsettings set org.gnome.meld style-scheme 'solarized-light'
    gsettings set org.gnome.meld use-system-font false
fi

if systemctl --user is-enabled conf-set.service; then
    myrun=$((myrun + 1))
    sed -i "3 s/^.*$/myrun=$myrun/" $HOME/.local/bin/conf-set.sh
    if [ "$myrun" -gt "2" ]; then
        systemctl --user disable conf-set.service && \
        echo "Unit conf-set.service disabled." && \
        sleep 5 && \
        notify-send -u critical -i $myinfoicon "conf-set.sh" "Unit conf-set.service disabled."
    fi
fi
