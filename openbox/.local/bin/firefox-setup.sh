#!/bin/bash

# Fedora

myfirefoxicon=firefox
myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myuser2=$(grep 1001 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/fedora/helpers"

if test -d /usr/lib64/firefox/distribution; then
    sudo cp $myhelp/policies.json /usr/lib64/firefox/distribution
fi

if which firefox | grep -q firefox; then
    echo "Firefox is installed."
    notify-send -u normal -i $myfirefoxicon "firefox-setup.sh" "Firefox is installed."
else
    echo "Firefox is not installed."
    notify-send -u critical -i $myfirefoxicon "firefox-setup.sh" "Firefox is not installed."
    exit
fi

[ -d ~/.mozilla ] && rm -rf ~/.mozilla
[ -d ~/.cache/mozilla ] && rm -rf ~/.cache/mozilla

echo "Launch Firefox, wait a few seconds and close it."
read -p "Press [ENTER] key to continue..."

if whoami | grep -q "$myuser1"; then
    cp $myhelp/user-1.js ~/.mozilla/firefox/$(ls ~/.mozilla/firefox | grep default-)/user.js
    cp $myhelp/bookmarks-1.json $(xdg-user-dir DESKTOP)/bookmarks.json
else
    cp $myhelp/user-2.js ~/.mozilla/firefox/$(ls ~/.mozilla/firefox | grep default-)/user.js
    cp $myhelp/bookmarks-2.json $(xdg-user-dir DESKTOP)/bookmarks.json
fi

echo "Launch Firefox again, restore bookmarks from backup then close it."
read -p "Press [ENTER] key to continue..."

sudo rm -f /usr/lib64/firefox/distribution/policies.json
rm -f ~/.mozilla/firefox/$(ls ~/.mozilla/firefox | grep default-)/user.js
rm -f $(xdg-user-dir DESKTOP)/bookmarks.json

echo -e "\n- Make DuckDuckGo default search engine\n- Set default zoom to 90%\n- Allow extensions to run in private mode"
notify-send -u critical -i $myfirefoxicon "firefox-setup.sh" "\- Make DuckDuckGo default search engine\n- Set default zoom to 90%\
\n- Allow extensions to run in private mode"
exit
