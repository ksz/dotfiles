#!/bin/bash

myinfoicon=dialog-information

sleep 5
notify-send -u critical -i $myinfoicon "first-run.sh" "Run scripts:\n- firewall-setup.sh\n- firefox-setup.sh"
systemctl --user disable first-run.service
