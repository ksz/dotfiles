#!/bin/bash

### retrieve the certificate from the VPN gateway:
myip=*.*.*.*
myport=****
echo | openssl s_client -connect $myip:$myport 2>/dev/null | \
  openssl x509 -outform der | \
  sha256sum | \
  awk '{ print $1 }' | \
  tr -d '\n' | \
  xclip -selection clipboard
echo "Certificate copied to clipboard."

### create a config file:
# cat << EOF | sudo tee -a /etc/openfortivpn/work-config
# host = ***
# port = ***
# username = ***
# password = ***
# set-dns = 0
# pppd-use-peerdns = 0
# trusted-cert = ***
# EOF
# sudo chmod 600 /etc/openfortivpn/work-config

### start openfortivpn:
# sudo openfortivpn -c /etc/openfortivpn/work-config

### stop openfortivpn:
# sudo killall openfortivpn



### journalctl -xe | grep "NetworkManager.*ERROR"
