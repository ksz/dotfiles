#!/bin/bash

sudo echo
test -d ~/.log || mkdir ~/.log

# mount -l | grep -q "/dev/nvme0n1*" || udisksctl mount -t ext4 -b /dev/nvme0n1*
# mount -l | grep -q "/dev/sda*" || udisksctl mount -t ntfs -b /dev/sda*

echo "*** $(date -R) ***" 2>&1 | tee -a ~/.log/fstrim-manual.log
sudo fstrim -a -v 2>&1 | tee -a ~/.log/fstrim-manual.log

# udisksctl unmount -b /dev/nvme0n1*
# udisksctl unmount -b /dev/sda*



### journalctl | grep "trimmed on"
