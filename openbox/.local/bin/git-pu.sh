#!/bin/bash

function_help () {
    echo "  -l, --pull     git pull"
    echo "  -s, --push     git push"
}

function_pull () {
    git pull origin main
}

function_push () {
    read -p "- Enter commit message: " mymsg
    git add . && echo && \
    git commit -m "$mymsg" && echo && \
    git push origin main
}

case "$1" in
    -l|--pull) function_pull ;;
    -s|--push) function_push ;;
    *        ) function_help ;;
esac
