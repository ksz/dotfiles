#! /bin/sh

# BIOS
mount /dev/sda1 /mnt
mount --bind /dev /mnt/dev
mount --bind /proc /mnt/proc
mount --bind /sys /mnt/sys
chroot /mnt <<EOF
    grub-install /dev/sda
    update-grub
EOF

# EFI
mount /dev/nvme0n1p2 /mnt
mount /dev/nvme0n1p1 /mnt/boot/efi
for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do mount -B $i /mnt$i; done
chroot /mnt <<EOF
    grub-install /dev/nvme0n1
    update-grub
EOF
