#!/bin/bash

myfile="$HOME/.local/share/hotkeys.txt"
mywindow=$(wmctrl -l -x | grep -E "yad.Yad .* Hotkeys$" | awk '{ print $1 }')

if [ -z "$mywindow" ]; then
    yad --text-info < $myfile --title=Hotkeys --window-icon=keyboard --width=445 --height=860 \
        --no-buttons --center --on-top --undecorated --fontname='Hack 9' # --close-on-unfocus
else
    wmctrl -i -c "$mywindow"
fi



# export GTK_THEME=Adwaita
# myfile="$HOME/.local/share/hotkeys.txt"
# mywindow=$(xdotool search --onlyvisible --class "zenity"; xdotool search --name "Hotkeys" | sort | uniq -d)
#
# if [ -z "$mywindow" ]; then
#     zenity --text-info --filename="$myfile" --title="Hotkeys" \
#            --width=450 --height=900 --font="Hack 9"
# else
#     xdotool windowkill "$mywindow"
# fi
