#!/bin/bash

while true; do
    # if grep -qr RUNNING /proc/asound; then
    if grep -q RUNNING /proc/asound/card*/pcm*/sub*/status; then
        pidof -s xautolock && xautolock -exit
    else
        pidof -s xautolock || xautolock -time 20 -corners -+00 -locker "xdotool mousemove_relative -- -300 300 && slock systemctl suspend" -detectsleep &
    fi
    sleep 300
done
