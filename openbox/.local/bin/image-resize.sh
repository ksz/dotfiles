#!/bin/bash

myimages=$(find . -type f -name "*.jpg" -size +400k)

for f in $myimages; do
    identify -format '%w %h\n' "$f" | \
    while read w h; do
        if [ $w -gt $h ]; then
            mogrify -resize 1200x800 "$f"   # landscape
        else
            mogrify -resize 800x1200 "$f"   # portrait
        fi
    done
done


# for f in $myimages; do mogrify -resize 50% "$f"; done
