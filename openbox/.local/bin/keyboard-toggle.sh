#!/bin/bash

mykeyboardicon=keyboard
mylayoutenicon=indicator-keyboard-En
mylayoutplicon=indicator-keyboard-Pl

if setxkbmap -query | grep -q "layout: .* us"; then
    setxkbmap -model pc105 -layout pl -option caps:swapescape
    if ps x | grep -q "[y]ad .* --text=Keyboard layout:"; then
        kill $(ps x | grep "[y]ad .* --text=Keyboard layout: EN" | awk '{ print $1 }')
    fi
    notify-send -u normal -i $mykeyboardicon -r 03 "keyboard-toggle.sh" "Keyboard layout has been changed to PL."
    yad --notification --image=$mylayoutplicon --text="Keyboard layout: PL" --menu="Toggle! keyboard-toggle.sh|Quit! quit" --command="menu"
else
    setxkbmap -model pc105 -layout us -option caps:swapescape
    if ps x | grep -q "[y]ad .* --text=Keyboard layout:"; then
        kill $(ps x | grep "[y]ad .* --text=Keyboard layout: PL" | awk '{ print $1 }')
    fi
    notify-send -u normal -i $mykeyboardicon -r 03 "keyboard-toggle.sh" "Keyboard layout has been changed to EN."
    yad --notification --image=$mylayoutenicon --text="Keyboard layout: EN" --menu="Toggle! keyboard-toggle.sh|Quit! quit" --command="menu"
fi
