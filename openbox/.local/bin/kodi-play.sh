#!/bin/bash

myip=192.168.*.*
myclip=$(xclip -o -selection clipboard)

function_play () {
    curl -X POST http://$myip:8080/jsonrpc -H "Content-Type: application/json" -d '{
      "jsonrpc": "2.0",
      "method": "Player.Open",
      "params": {
        "item": {
          "file": "plugin://plugin.video.youtube/?action=play_video&videoid='"$myid"'"
        }
      },
      "id": 1
    }'
}

function_error () {
    curl -X POST http://$myip:8080/jsonrpc -H "Content-Type: application/json" -d '{
      "jsonrpc": "2.0",
      "method": "GUI.ShowNotification",
      "params": {
        "title": "YouTube",
        "message": "Missing video ID..."
      },
      "id": 1
    }'
}

if echo "$myclip" | grep -q "youtube.com"; then
    myid=$(echo "$myclip" | cut -d \= -f 2)
    function_play
elif echo "$myclip" | grep -q "youtu.be"; then
    myid=$(echo "$myclip" | cut -d \/ -f 4)
    function_play
else
    function_error
fi

echo



# https://youtu.be/**********
# https://www.youtube.com/watch?v=**********
