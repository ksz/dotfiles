#!/bin/bash

function_menu () {
    choices="shutdown\nreboot\nsuspend\nlogout\ndualboot\nschedule\nlock"
    chosen=$(echo -e "$choices" | rofi -dmenu -i -l 7 -theme-str 'window {width: 6%;}' -p leave)
    case "$chosen" in
        shutdown) leave-out.sh -p ;;
        reboot  ) leave-out.sh -r ;;
        suspend ) leave-out.sh -s ;;
        logout  ) leave-out.sh -e ;;
        dualboot) leave-out.sh -d ;;
        schedule) leave-out.sh -h ;;
        lock    ) leave-out.sh -l ;;
    esac
}

case "$1" in
    -p) systemctl poweroff ;;
    -r) systemctl reboot ;;
    -s) slock systemctl suspend ;;
    -e) openbox --exit && ps -u $USER -o pid= | xargs kill -9 ;;
    -d) sudo grub2-reboot 3 && systemctl reboot ;;
    -h) st -f Hack -g 45x15 -e shutdown-schedule.sh ;;
    -l) slock ;;
    -m) function_menu ;;
esac
