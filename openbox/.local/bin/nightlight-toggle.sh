#!/bin/bash

myscreencoloricon=redshift

pkill -USR1 ^redshift$ && \
notify-send -u normal -i $myscreencoloricon "nightlight-toggle.sh" "Screen color temperature has been changed."
