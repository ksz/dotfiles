#!/bin/bash

echo "- Choose source:"
echo "  1) channel 2) playlist"
while true; do
    read -p "  Enter a number: " mysource
    case "$mysource" in
        1) mysource=channel; break ;;
        2) mysource=playlist; break ;;
        *) echo "  Invalid input - try again."
    esac
done

echo "- Choose player:"
echo "  1) kodi 2) generic"
while true; do
    read -p "  Enter a number: " myplayer
    case "$myplayer" in
        1) myplayer=kodi; break ;;
        2) myplayer=generic; break ;;
        *) echo "  Invalid input - try again."
    esac
done

read -p "- Enter playlist name: " myplaylist

if [ "$mysource" = "channel" ]; then
    read -p "- Enter channel name: " mychannel
    yt-dlp --flat-playlist --print-json https://www.youtube.com/@$mychannel/videos | jq -r '.title,.url' > "$myplaylist.m3u"
else
    read -p "- Enter playlist ID: " myid
    yt-dlp --flat-playlist --print-json https://www.youtube.com/playlist?list=$myid | jq -r '.title,.url' > "$myplaylist.m3u"
fi

sed -i "s/^/#EXTINF:-1,/" "$myplaylist.m3u"

sed -i "s/^#EXTINF:-1,https:/https:/" "$myplaylist.m3u"

sed -i "1i#EXTM3U" "$myplaylist.m3u"

if [ "$myplayer" = "kodi" ]; then
    sed -i "s|https://www.youtube.com/watch?v=|plugin://plugin.video.youtube/play/?video_id=|" "$myplaylist.m3u"
fi
