#!/bin/bash

myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/$1/helpers"

if [ "$1" = "arch" ]; then
    cp $myhelp/.{bash_profile,bashrc,xprofile} ~/.
elif [ "$1" = "debian" ]; then
    cp -f $myhelp/.{bash_aliases,xsessionrc} ~/.
    sed -i "s/_Light/_Snow/;s/22/16/" ~/.{config/gtk-3.0/settings.ini,gtkrc-2.0,Xdefaults,Xresources}
else
    cp -f $myhelp/.bashrc ~/.
fi

xdg-user-dirs-update
ln -sf $myhelp/$2-wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
ln -sf $myhelp/$2-wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg
find ~/.config/nsxiv/exec -type f -exec chmod +x "{}" \;
find ~/.local/bin -type f -exec chmod +x "{}" \;
chmod +x ~/.local/share/kio/servicemenus/*.desktop
echo '#!/usr/bin/env python3' | tee "$(xdg-user-dir TEMPLATES)/Python Script....py"
echo '#!/bin/bash' | tee "$(xdg-user-dir TEMPLATES)/Shell Script....sh"
systemctl --user enable conf-set.service
systemctl --user enable first-run.service

sudo mkdir -p /root/.config/gtk-3.0
sudo cp -rf ~/.config/mc /root/.config
sudo cp -f ~/.config/gtk-3.0/settings.ini /root/.config/gtk-3.0
sudo sed -i "s/modarin256/modarin256root/" /root/.config/mc/ini

if grep -q VIRT /etc/hostname; then
    sudo cp -f $myhelp/00-keyboard.conf /etc/X11/xorg.conf.d
    sed -i "/picom/s/^/# /;/redshift/s/^/# /" ~/.config/openbox/autostart
    sed -i "/idle-watch.sh/s/^/# /;/password-backup.sh/s/^/# /" ~/.config/openbox/autostart     #TODO: password-backup > backup-manual
    echo "xset s off -dpms" | tee -a ~/.config/openbox/autostart
fi

if whereis -b k3b | grep -q "k3b: /"; then
    sudo usermod -aG cdrom "$myuser"
    sudo chown root:cdrom /usr/bin/{cdrdao,growisofs,wodim}
    sudo chmod 4710 /usr/bin/{cdrdao,wodim}
    sudo chmod 0750 /usr/bin/growisofs
fi
