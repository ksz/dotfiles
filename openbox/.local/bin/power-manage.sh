#!/bin/bash

myscreensavericon=preferences-desktop-screensaver
myidleicon=idle
mypowericon=preferences-system-power

function_dpms () {
    if xset q | grep -q "timeout:.*600.*cycle:.*600"; then
        xset s 0 0; xset dpms 0 0 0
        notify-send -u normal -i $myscreensavericon -r 03 "power-manage.sh" "Screensaver has been disabled."
        yad --notification --image=$myscreensavericon --text="Screensaver disabled" --menu="Turn on! power-manage.sh -d" --command="menu"
    else
        xset s 600 600; xset dpms 600 600 600
        notify-send -u normal -i $myscreensavericon -r 03 "power-manage.sh" "Screensaver has been enabled."
        kill $(ps x | grep "[y]ad .* --text=Screensaver disabled" | awk '{ print $1 }')
    fi
}

# function_dpms () {
#     if xset q | grep -q "DPMS is Enabled"; then
#         xset s off -dpms
#         notify-send -u normal -i $myscreensavericon -r 03 "power-manage.sh" "DPMS has been disabled."
#     else
#         xset s on +dpms
#         # xset s default +dpms
#         notify-send -u normal -i $myscreensavericon -r 03 "power-manage.sh" "DPMS has been enabled."
#     fi
# }

function_idle () {
    if pgrep idle-watch.sh; then
        pidof -s xautolock && xautolock -exit
        for i in $(pstree -p $(pgrep -o idle-watch.sh) | grep -o '(\([0-9]*\))' | cut -d'(' -f2 | cut -d')' -f1); do
            kill $i
        done
        notify-send -u normal -i $myidleicon -r 03 "power-manage.sh" "Idleness watcher has been disabled."
        yad --notification --image=$myidleicon --text="Idleness watcher disabled" --menu="Turn on! power-manage.sh -i" --command="menu"
    else
        idle-watch.sh &
        notify-send -u normal -i $myidleicon -r 03 "power-manage.sh" "Idleness watcher has been enabled."
        kill $(ps x | grep "[y]ad .* --text=Idleness watcher disabled" | awk '{ print $1 }')
    fi
}

function_monitor () {
    sleep 1; xset dpms force off
}

function_present () {
    if pgrep idle-watch.sh; then
        pidof -s xautolock && xautolock -exit
        for i in $(pstree -p $(pgrep -o idle-watch.sh) | grep -o '(\([0-9]*\))' | cut -d'(' -f2 | cut -d')' -f1); do
            kill $i
        done
        xset s 0 0; xset dpms 0 0 0
        notify-send -u normal -i $mypowericon -r 03 "power-manage.sh" "Presentation mode has been enabled."
        yad --notification --image=$mypowericon --text="Presentation mode enabled" --menu="Turn off! power-manage.sh -p" --command="menu"
    else
        idle-watch.sh &
        xset s 600 600; xset dpms 600 600 600
        notify-send -u normal -i $mypowericon -r 03 "power-manage.sh" "Presentation mode has been disabled."
        kill $(ps x | grep "[y]ad .* --text=Presentation mode enabled" | awk '{ print $1 }')
    fi
}

function_menu () {
    choices="present\nidle\ndpms\nmonitor"
    chosen=$(echo -e "$choices" | rofi -dmenu -i -l 4 -theme-str 'window {width: 6%;}' -p power)
    case "$chosen" in
        present) function_present ;;
        idle   ) function_idle ;;
        dpms   ) function_dpms ;;
        monitor) function_monitor ;;
    esac
}

case "$1" in
    -d) function_dpms ;;
    -i) function_idle ;;
    -o) function_monitor ;;
    -p) function_present ;;
    -m) function_menu ;;
esac
