#!/bin/bash

myfile=$(xdg-user-dir DESKTOP)/ss.$(date +'%Y%m%d.%H%M%S').png
mysound=/usr/share/sounds/freedesktop/stereo/camera-shutter.oga

case "$1" in
    -a) maim -ui $(xdotool getactivewindow) $myfile ;;
    -f) maim -u $myfile ;;
    -s) maim -us $myfile ;;
#     -a) scrot -u $myfile ;;
#     -f) scrot $myfile ;;
#     -s) scrot -s $myfile ;;
esac

if [ $? -eq 0 ]; then pw-cat -p $mysound; fi
# if [ $? -eq 0 ]; then paplay $mysound; fi

echo $myfile | xclip -selection clipboard
