#!/bin/bash

mydotfiles=https://gitlab.com/ksz/dotfiles.git
mysetup=https://gitlab.com/ksz/setup.git
myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)

if tty | grep -q /dev/pts; then
    echo "Log out and switch to tty3 (Ctrl+Alt+F3)."
    exit
fi

echo "- Choose distro:"
echo "  1) Arch 2) Artix 3) Debian 4) Devuan 5) Fedora"
while true; do
    read -p "  Enter a number: " mydistro </dev/tty
    case "$mydistro" in
        1) mydistro1=arch && mydistro2=arch; break ;;
        2) mydistro1=arch && mydistro2=artix; break ;;
        3) mydistro1=debian && mydistro2=debian; break ;;
        4) mydistro1=debian && mydistro2=devuan; break ;;
        5) mydistro1=fedora && mydistro2=fedora; break ;;
        *) echo "  Invalid input - try again."
    esac
done

echo "- Choose DE/WM:"
echo "  1) lxqt 2) mate 3) xfce 4) dwm 5) labwc 6) openbox"
while true; do
    read -p "  Enter a number: " mydewm </dev/tty
    case "$mydewm" in
        1) mydewm=lxqt; break ;;
        2) mydewm=mate; break ;;
        3) mydewm=xfce; break ;;
        4) mydewm=dwm; break ;;
        5) mydewm=labwc; break ;;
        6) mydewm=openbox; break ;;
        *) echo "  Invalid input - try again."
    esac
done

sudo mv /home/$myuser1 /home/$myuser1.$(date +'%Y%m%d')
sudo mkhomedir_helper "$myuser1"
sudo chmod go-r-x /home/$myuser1
mkdir -p ~/.local/src
cp /home/$myuser1.$(date +'%Y%m%d')/.local/src/setup.log ~/.local/src
git clone --depth 1 "$mydotfiles" ~/.local/src/dotfiles
git clone --depth 1 "$mysetup" ~/.local/src/setup
find ~/.local/src/dotfiles -type f ! -name "setup-*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
find ~/.local/src/setup -type f ! -name "setup*.sh" ! -path "*/.git/*" -exec sed -i "s/<USER>/$myuser1/" "{}" \;
rsync -r ~/.local/src/dotfiles/$mydewm/ ~/.
bash post-install.sh "$mydistro1" "$mydistro2"

#TODO
# if grep -q OP7050 /etc/hostname; then
#     echo TODO
# fi
