#!/bin/bash

myvideoicon=video-display

function_single () {
    for i in $(xrandr -q | grep " connected" | cut -d " " -f1); do xrandr --output $i --off; done
    xrandr --output DP1 --primary --mode 1920x1200 --pos 0x0 --rotate normal --output DP2 --off --output DP3 --off --output HDMI1 --off --output HDMI2 --off --output HDMI3 --off --output VIRTUAL1 --off
    feh --bg-center $(xdg-user-dir PICTURES)/wallpaper.jpg
    audio-toggle.sh --displayport1
    pgrep redshift >/dev/null || redshift &
    notify-send -t 10000 -u normal -i $myvideoicon "video-toggle.sh" "Video output: EIZO 24\""
    mywindowposX=100; mywindowposY=100
    function_position
}

function_double () {
    for i in $(xrandr -q | grep " connected" | cut -d " " -f1); do xrandr --output $i --off; done
    xrandr --output DP1 --primary --mode 1920x1200 --pos 0x720 --rotate normal --output DP2 --mode 1920x1080 --pos 1920x0 --rotate left --output DP3 --off --output HDMI1 --off --output HDMI2 --off --output HDMI3 --off --output VIRTUAL1 --off
    feh --bg-center $(xdg-user-dir PICTURES)/wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg
    audio-toggle.sh --displayport2
    pgrep redshift >/dev/null || redshift &
    notify-send -t 10000 -u normal -i $myvideoicon "video-toggle.sh" "Video output: EIZO 24\" + EIZO 23\""
    mywindowposX=100; mywindowposY=820
    function_position
}

function_triple () {
    for i in $(xrandr -q | grep " connected" | cut -d " " -f1); do xrandr --output $i --off; done
    xrandr --output DP1 --primary --mode 1920x1200 --pos 1920x720 --rotate normal --output DP2 --mode 1920x1080 --pos 3840x0 --rotate left --output DP3 --off --output HDMI1 --mode 1920x1080 --pos 0x840 --rotate normal --output HDMI2 --off --output HDMI3 --off --output VIRTUAL1 --off
    feh --bg-center $(xdg-user-dir PICTURES)/wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
    audio-toggle.sh --displayport1
    pgrep redshift >/dev/null || redshift &
    notify-send -t 10000 -u normal -i $myvideoicon "video-toggle.sh" "Video output: EIZO 24\" + EIZO 23\" + SAMSUNG"
    mywindowposX=2020; mywindowposY=820
    function_position
}

function_tvset () {
    for i in $(xrandr -q | grep " connected" | cut -d " " -f1); do xrandr --output $i --off; done
    xrandr --output DP1 --off --output DP2 --off --output DP3 --off --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI2 --off --output HDMI3 --off --output VIRTUAL1 --off
    feh --bg-center $(xdg-user-dir PICTURES)/wallpaper.jpg
    audio-toggle.sh --hdmi
    pgrep redshift >/dev/null && killall redshift
    notify-send -t 10000 -u normal -i $myvideoicon "video-toggle.sh" "Video output: SAMSUNG"
    mywindowposX=100; mywindowposY=100
    function_position
}

function_position () {
    for i in $(wmctrl -l -G | grep -v -E " tint2$| conky \(| 1920 " | cut -d " " -f1); do
        xdotool windowactivate --sync $i
        xdotool getactivewindow windowmove --sync $mywindowposX $mywindowposY
        xdotool windowminimize --sync $i
        mywindowposX=$((mywindowposX + 50))
        mywindowposY=$((mywindowposY + 50))
    done
    pgrep conky >/dev/null && killall conky && conky --daemonize --pause=5 # -x $myconkyposX -y $myconkyposY
    ### conky with 'dock' window type
    # sed -i "/<floatingX>/s/[0-9]\+/$myconkyposX/;/<floatingY>/s/[0-9]\+/$myconkyposY/" ~/.config/openbox/rc.xml
    # openbox --reconfigure
}

function_menu () {
    choices="single\ndouble\ntriple\ntvset"
    chosen=$(echo -e "$choices" | rofi -dmenu -i -l 4 -theme-str 'window {width: 6%;}' -p video)
    case "$chosen" in
        single) function_single ;;
        double) function_double ;;
        triple) function_triple ;;
        tvset ) function_tvset ;;
    esac
}

case "$1" in
    -s|--single) function_single ;;
    -d|--double) function_double ;;
    -t|--triple) function_triple ;;
    -v|--tvset ) function_tvset ;;
    -m|--menu  ) function_menu ;;
esac



# xrandr -q
# Screen 0: minimum 8 x 8, current 1920 x 1080, maximum 32767 x 32767
# Screen 0: minimum 8 x 8, current 1920 x 1200, maximum 32767 x 32767
# Screen 0: minimum 8 x 8, current 3000 x 1920, maximum 32767 x 32767
# Screen 0: minimum 8 x 8, current 4920 x 1920, maximum 32767 x 32767

# xrandr --listmonitors
# 0: +*DP1 1920/520x1200/330+1920+720  DP1
# 1: +DP2 1080/510x1920/290+3840+0  DP2
# 2: +HDMI1 1920/700x1080/390+0+840  HDMI1
