#!/bin/bash

myvolumemuteicon=volume-level-muted
myvolumehighicon=volume-level-high

function_notify () {
    if wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q "\[MUTED\]"; then
        notify-send -t 1000 -u low -i $myvolumemuteicon -r 02 "volume-set.sh" "Volume: MUTE"
    else
        myvolume=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | sed "s/Volume: //")
        myvolume=$(echo "scale=0;($myvolume*100)/1" | bc)
        notify-send -t 1000 -u low -i $myvolumehighicon -r 02 "volume-set.sh" "Volume: $myvolume%"
    fi
}

case $1 in
    -d) wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%- ;;
    -i) wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 10%+ ;;
    -f) wpctl set-volume @DEFAULT_AUDIO_SINK@ 100% ;;
    -m) wpctl set-mute @DEFAULT_AUDIO_SINK@ 1 ;;
    -u) wpctl set-mute @DEFAULT_AUDIO_SINK@ 0 ;;
    -t) wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle ;;
esac

if [ $? -eq 0 ]; then function_notify; fi
