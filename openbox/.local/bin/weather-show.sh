#!/bin/bash

myopenweatherapikey=

function_bochnia () {
    if curl -s --retry 5 --retry-max-time 30 --url "https://api.open-meteo.com/v1/forecast?latitude=49.9691&longitude=20.4303&current=temperature_2m,wind_speed_10m&timezone=Europe%2FBerlin" | jq -r '[ .current.temperature_2m,.current_units.temperature_2m,.current.wind_speed_10m,.current_units.wind_speed_10m ] | join(" ")' | while read i j k l; do echo " $i $j   $k $l"; done | grep "[0-9] km/h"; then
        true
    elif curl -s wttr.in/Bochnia?format="+%t+%w" | grep "[0-9]km/h"; then
        true
    elif
        curl -s "https://api.openweathermap.org/data/2.5/weather?id=7532635&units=metric&appid=$myopenweatherapikey" | jq -r '[ .main.temp,.wind.speed ] | join(" ")' | while read i j; do echo " $i ℃   $j m/s"; done | grep "[0-9] m/s"; then
        true
    else
        echo " N/A"
    fi
}

function_krakow () {
    if curl -s --retry 5 --retry-max-time 30 --url "https://api.open-meteo.com/v1/forecast?latitude=50.0614&longitude=19.9366&current=temperature_2m,wind_speed_10m&timezone=Europe%2FBerlin" | jq -r '[ .current.temperature_2m,.current_units.temperature_2m,.current.wind_speed_10m,.current_units.wind_speed_10m ] | join(" ")' | while read i j k l; do echo " $i $j   $k $l"; done | grep "[0-9] km/h"; then
        true
    elif curl -s wttr.in/Kraków?format="+%t+%w" | grep "[0-9]km/h"; then
        true
    elif
        curl -s "https://api.openweathermap.org/data/2.5/weather?id=3094802&units=metric&appid=$myopenweatherapikey" | jq -r '[ .main.temp,.wind.speed ] | join(" ")' | while read i j; do echo " $i ℃   $j m/s"; done | grep "[0-9] m/s"; then
        true
    else
        echo " N/A"
    fi
}

case "$1" in
    -b) function_bochnia ;;
    -k) function_krakow ;;
esac
