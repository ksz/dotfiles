#!/bin/bash

mywindows=$(wmctrl -l -x | grep -v -E " tint2$| conky \(| CopyQ$" | awk '{ print $3 }')

mywindow=$(echo $mywindows | cut -d ' ' -f$1)

wmctrl -x -a $mywindow
