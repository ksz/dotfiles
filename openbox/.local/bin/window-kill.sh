#!/bin/bash

for i in $(wmctrl -l | grep -v -E " tint2$| conky \(| CopyQ$" | awk '{ print $1 }'); do
    wmctrl -i -a $i && wmctrl -i -c $i
done
