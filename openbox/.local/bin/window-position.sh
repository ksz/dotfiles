#!/bin/bash

myfiles=$(grep -R "XPosition=" ~/.local/state | cut -d \: -f1)

if [ -n "$myfiles" ]; then
    for i in $myfiles; do
        sed -i "/Position=/d" "$i"
    done
else
    echo "No files to process."
fi
