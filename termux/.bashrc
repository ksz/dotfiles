export EDITOR=/usr/bin/nano
export PATH="$PATH:$HOME/bin"

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
