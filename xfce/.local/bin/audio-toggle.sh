#!/bin/bash

# OP7050 (Eizo <---> Samsung)
# if pactl get-default-sink | grep -q "alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1"; then
    # pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo
    # pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo
    # pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo hdmi-output-0
    # pactl set-sink-mute @DEFAULT_SINK@ false
    # pactl set-sink-volume @DEFAULT_SINK@ 100%
    # notify-send -t 5000 -u normal "audio-toggle.sh" "Audio device:\n SAMSUNG"
# else
    # pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo-extra1
    # pactl set-default-sink alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1
    # pactl set-sink-port alsa_output.pci-0000_00_1f.3.hdmi-stereo-extra1 hdmi-output-1
    # pactl set-sink-mute @DEFAULT_SINK@ false
    # pactl set-sink-volume @DEFAULT_SINK@ 100%
    # notify-send -t 5000 -u normal "audio-toggle.sh" "Audio device:\n EIZO"
# fi



# pacmd list-cards
# pacmd list-sinks

# pactl set-sink-volume @DEFAULT_SINK@ 100%
# pactl set-sink-volume @DEFAULT_SINK@ +10%
# pactl set-sink-volume @DEFAULT_SINK@ -10%
# pactl set-sink-mute @DEFAULT_SINK@ toggle

# wpctl set-volume @DEFAULT_AUDIO_SINK@ 100%
# wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%+
# wpctl set-volume @DEFAULT_AUDIO_SINK@ 10%-
# wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
