#!/bin/bash

myrun=0
myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)

if uname -a | grep -iq debian; then mydistro=debian; else mydistro=arch; fi
myhelp="$HOME/.local/src/setup/$mydistro/helpers"

grep -E -v '^# |^$' $myhelp/backup-xfconf | while IFS=';' read i j k l; do xfconf-query -c $i -np $j -t $k -s "$l"; done
grep -E -v '^# |^$' $myhelp/backup-gsettings | while IFS=' ' read i j k; do gsettings set $i $j "$k"; done
xfconf-query -c xfce4-desktop -np $(xfconf-query -c xfce4-desktop -l | grep workspace0/last-image) -t string -s $(xdg-user-dir PICTURES)/wallpaper.jpg

if whoami | grep -q "$myuser"; then
    echo "Current user: $myuser"
else
    echo "Current user: $(whoami)"
    xfconf-query -c xfce4-session -np /shutdown/LockScreen -t bool -s false
    xfconf-query -c xfwm4 -np /general/scroll_workspaces -t bool -s false
    xfconf-query -c xfce4-panel -np /panels/panel-1/size -t uint -s 30
fi

# TODO: arch vs debian --- xfce-screesaver vs light-locker
if grep -q VIRT /etc/hostname; then
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/blank-on-ac -t int -s 0
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/dpms-on-ac-off -t uint -s 0
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/dpms-on-ac-sleep -t uint -s 0
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/inactivity-on-ac -t uint -s 0
fi

if systemctl --user is-enabled conf-set.service; then
    myrun=$((myrun + 1))
    sed -i "3 s/^.*$/myrun=$myrun/" $HOME/.local/bin/conf-set.sh
    if [ "$myrun" -gt "2" ]; then
        systemctl --user disable conf-set.service && \
        echo "Unit conf-set.service disabled." && \
        sleep 5 && \
        notify-send -t 10000 -u normal "conf-set.sh" "Unit conf-set.service disabled."
    fi
fi
