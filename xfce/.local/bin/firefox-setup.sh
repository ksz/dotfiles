#!/bin/bash

# Debian

myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myuser2=$(grep 1001 /etc/passwd | cut -d \: -f 1)
# myhelp="$HOME/.local/src/setup/debian/helpers"
myhelp="$HOME/.local/src/setup/fedora/helpers"

if test -d /usr/lib/firefox-esr/distribution; then
    sudo cp $myhelp/policies.json /usr/lib/firefox-esr/distribution
fi

if which firefox-esr | grep -q firefox-esr; then
    echo "Firefox setup is starting ..."
    notify-send -t 10000 -u normal "firefox-setup.sh" "Firefox setup is starting ..."
else
    echo "Firefox is not installed."
    notify-send -t 3600000 -u normal "firefox-setup.sh" "Firefox is not installed."
    exit
fi

[ -d ~/.mozilla ] && rm -rf ~/.mozilla
[ -d ~/.cache/mozilla ] && rm -rf ~/.cache/mozilla
firefox-esr &
sleep 20
wmctrl -c Firefox
pkill -f firefox-esr

if whoami | grep -q "$myuser1"; then
    cp $myhelp/user-1.js ~/.mozilla/firefox/$(ls ~/.mozilla/firefox | grep default-esr)/user.js
    cp $myhelp/bookmarks-1.json $(xdg-user-dir DESKTOP)/bookmarks.json
else
    cp $myhelp/user-2.js ~/.mozilla/firefox/$(ls ~/.mozilla/firefox | grep default-esr)/user.js
    cp $myhelp/bookmarks-2.json $(xdg-user-dir DESKTOP)/bookmarks.json
fi

sleep 5
firefox-esr &
sleep 5 && xdotool key ctrl+shift+o
sleep 3 && xdotool key alt+i
sleep 1.5 && xdotool key Down
sleep 1.5 && xdotool key Right
sleep 1.5 && xdotool key Return
sleep 2.5 && xdotool key End
sleep 2.5 && xdotool key Return
sleep 2.5 && xdotool key Return
sleep 2 && xdotool key ctrl+w
sleep 2 && wmctrl -c Firefox
pkill -f firefox-esr

sudo bash -c 'cat << EOF > /usr/lib/firefox-esr/distribution/policies.json
{
  "policies": {
    "SearchEngines": {
      "Default": "DuckDuckGo"
    }
  }
}
EOF'

rm -f ~/.mozilla/firefox/$(ls ~/.mozilla/firefox | grep default-esr)/user.js
rm -f $(xdg-user-dir DESKTOP)/bookmarks.json

echo -e "\n- Make Firefox default browser\n- Set default zoom to 90%\n- Allow extensions to run in private mode"
notify-send -t 3600000 -u normal "firefox-setup.sh" "\- Make Firefox default browser\n- Set default zoom to 90%\
\n- Allow extensions to run in private mode"
exit
