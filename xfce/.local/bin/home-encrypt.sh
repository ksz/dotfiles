#!/bin/bash

if whoami | grep -q root; then
    # login as root:
    myuser1=$(grep 1000 /etc/passwd | cut -d \: -f 1)
    apt-get -y install ecryptfs-utils rsync lsof cryptsetup
    modprobe ecryptfs
    ecryptfs-migrate-home -u $myuser1
    ecryptfs-setup-swap
else
    # login as myuser1 (BEFORE rebooting the computer):
    ecryptfs-unwrap-passphrase
    sudo rm -rf /home/$(whoami).*
fi
