#!/bin/bash

myuser=$(grep 1000 /etc/passwd | cut -d \: -f 1)
myhelp="$HOME/.local/src/setup/$1/helpers"

if [ "$1" = "arch" ]; then
    cp $myhelp/.{bash_profile,bashrc,xprofile} ~/.
elif [ "$1" = "debian" ]; then
    cp $myhelp/.{bash_aliases,xsessionrc} ~/.
    sed -i "/xfce4-power-manager/s/^# //" ~/.xsessionrc
else
    cp $myhelp/.{bash_profile,bashrc} ~/.
fi

xdg-user-dirs-update
ln -sf $myhelp/$2-wallpaper.jpg $(xdg-user-dir PICTURES)/wallpaper.jpg
ln -sf $myhelp/$2-wallpaper-rotate.jpg $(xdg-user-dir PICTURES)/wallpaper-rotate.jpg
# # xfce4-panel-profiles load $myhelp/panel-xfce.tar.bz2
grep -E -v '^# |^$' $myhelp/backup-xfconf | while IFS=';' read i j k l; do xfconf-query -c $i -np $j -t $k -s "$l"; done
grep -E -v '^# |^$' $myhelp/backup-gsettings | while IFS=' ' read i j k; do gsettings set $i $j "$k"; done
xfconf-query -c xfce4-desktop -np $(xfconf-query -c xfce4-desktop -l | grep workspace0/last-image) -t string -s "$(xdg-user-dir PICTURES)/wallpaper.jpg"
chmod +x ~/.local/bin/*.sh

if whoami | grep -q "$myuser"; then
    echo "Current user: $myuser"
    sudo mkdir /root/.config
    sudo cp -r ~/.config/{htop,mc} /root/.config
    sudo sed -i "/xfce-shapes.svg/s/^# //" /etc/lightdm/lightdm-gtk-greeter.conf
    if which gtk3-nocsd | grep -q gtk3-nocsd; then
        sudo apt-get -y purge gtk3-nocsd
        sudo apt-get -y autoremove
    fi
    if grep -q VIRT /etc/hostname; then
        # sudo cp /etc/default/keyboard /etc/default/keyboard.bak
        # sudo sed -i '/^XKBLAYOUT=/s/".*"/"pl,us"/' /etc/default/keyboard
        # sudo sed -i '/XKBOPTIONS=""/s/""/"caps:swapescape"/' /etc/default/keyboard
        sudo cp $myhelp/00-keyboard.conf /etc/X11/xorg.conf.d
    fi
else
    echo "Current user: $(whoami)"
    sed -i "/Hidden=/s/false/true/" ~/.config/autostart/conky.desktop
    xfconf-query -c xfce4-session -np /shutdown/LockScreen -t bool -s false && \
    echo "Lock screen disabled." && \
    notify-send -t 3600000 -u normal "post-install.sh" "Lock screen disabled."
    xfconf-query -c xfwm4 -np /general/scroll_workspaces -t bool -s false && \
    echo "Switching workspaces with scroll disabled." && \
    notify-send -t 3600000 -u normal "post-install.sh" "Switching workspaces with scroll disabled."
    xfconf-query -c xfce4-panel -np /panels/panel-1/size -t uint -s 30 && \
    echo "Panel row size changed to 30 pixels." && \
    notify-send -t 3600000 -u normal "post-install.sh" "Panel row size changed to 30 pixels."
fi

if grep -q VIRT /etc/hostname; then
    # sed -i "/set-sink-mute/s/false/true/;/set-sink-volume/s/^/# /" ~/.config/pulse/default.pa && \
    # echo "Sound muted." && \
    # notify-send -t 3600000 -u normal "post-install.sh" "Sound muted."
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/blank-on-ac -t int -s 0 && \
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/dpms-on-ac-off -t uint -s 0 && \
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/dpms-on-ac-sleep -t uint -s 0 && \
    xfconf-query -c xfce4-power-manager -np /xfce4-power-manager/inactivity-on-ac -t uint -s 0 && \
    xfconf-query -c xfce4-session -np /shutdown/LockScreen -t bool -s false && \
    echo "Automatic sleep disabled." && \
    notify-send -t 3600000 -u normal "post-install.sh" "Automatic sleep disabled."
fi

if env | grep -q LANG=en_US.UTF-8; then
    ln -sf ~/.config/gtk-3.0/bookmarks.eng.bak ~/.config/gtk-3.0/bookmarks && \
    ln -sf ~/.config/Thunar/uca.xml.eng.bak ~/.config/Thunar/uca.xml && \
    echo '#!/bin/bash' | tee $(xdg-user-dir TEMPLATES)/Script.sh && \
    echo "The main system language is English." && \
    notify-send -t 3600000 -u normal "post-install.sh" "The main system language is English."
else
    ln -sf ~/.config/gtk-3.0/bookmarks.pol.bak ~/.config/gtk-3.0/bookmarks && \
    ln -sf ~/.config/Thunar/uca.xml.pol.bak ~/.config/Thunar/uca.xml && \
    echo '#!/bin/bash' | tee $(xdg-user-dir TEMPLATES)/Skrypt.sh && \
    echo "The main system language is Polish." && \
    notify-send -t 3600000 -u normal "post-install.sh" "The main system language is Polish."
fi

echo

if which avahi-discover | grep -q avahi-discover; then
    echo "Avahi Zeroconf Browser is installed."
else
    rm -f ~/.local/share/applications/avahi-discover.desktop && \
    echo "Avahi Zeroconf Browser is not installed."
fi

if which bssh | grep -q bssh; then
    echo "Avahi SSH Server Browser is installed."
else
    rm -f ~/.local/share/applications/bssh.desktop && \
    echo "Avahi SSH Server Browser is not installed."
fi

if which bvnc | grep -q bvnc; then
    echo "Avahi VNC Server Browser is installed."
else
    rm -f ~/.local/share/applications/bvnc.desktop && \
    echo "Avahi VNC Server Browser is not installed."
fi

if which balena-etcher | grep -q balena-etcher; then
    echo "balenaEtcher is installed."
else
    rm -rf ~/.config/balena-etcher && \
    echo "balenaEtcher is not installed."
fi

if which brasero | grep -q brasero; then
    sed -i "/NoDisplay=/s/false/true/" ~/.local/share/applications/xfburn.desktop && \
    echo "Brasero is installed."
else
    echo "Brasero is not installed."
fi

if which catfish | grep -q catfish; then
    xfconf-query -c catfish -np /show-hidden-files -t bool -s true && \
    sed -i "/^<\!--$/d;/^-->$/d" ~/.config/Thunar/uca.xml.{eng,pol}.bak && \
    echo "Catfish is installed."
else
    echo "Catfish is not installed."
fi

if which celluloid | grep -q celluloid; then
    gsettings set io.github.celluloid-player.Celluloid csd-enable false && \
    gsettings set io.github.celluloid-player.Celluloid mpv-config-enable true && \
    gsettings set io.github.celluloid-player.Celluloid mpv-config-file '/home/<USER>/.config/mpv/mpv.conf' && \
    sed -i "/^video/s/mpv/io.github.celluloid_player.Celluloid/" ~/.config/mimeapps.list && \
    sed -i "/NoDisplay=/s/false/true/" ~/.local/share/applications/mpv.desktop && \
    echo "Celluloid is installed."
else
    echo "Celluloid is not installed."
fi

if which conky | grep -q conky; then
    echo "Conky is installed."
else
    rm -f ~/.config/autostart/conky.desktop && \
    rm -rf ~/.config/conky && \
    rm -f ~/.local/share/applications/conky.desktop && \
    echo "Conky is not installed."
fi

if test -d /opt/deadbeef; then
    sed -i "/^audio/s/mpv/deadbeef/" ~/.config/mimeapps.list && \
    echo "DeaDBeeF is installed."
else
    rm -rf ~/.config/deadbeef && \
    echo "DeaDBeeF is not installed."
fi

if test -f /opt/freac.AppImage; then
    sed -i "/Hidden=/s/true/false/" ~/.local/share/applications/freac.desktop && \
    echo "fre:ac is installed."
else
    echo "fre:ac is not installed."
fi

if which geany | grep -q geany; then
    echo "Geany is installed."
else
    rm -rf ~/.config/geany && \
    echo "Geany is not installed."
fi

if which htop | grep -q htop; then
    echo "htop is installed."
else
    rm -rf ~/.config/htop && \
    rm -f ~/.local/share/applications/htop.desktop && \
    echo "htop is not installed."
fi

if which keepassxc | grep -q keepassxc; then
    echo "KeePassXC is installed."
else
    rm -rf ~/.config/keepassxc && \
    echo "KeePassXC is not installed."
fi

if which libreoffice | grep -q libreoffice; then
    cp /usr/share/applications/libreoffice-*.desktop ~/.local/share/applications && \
    ls ~/.local/share/applications/libreoffice-* | xargs sed -i "/^Exec=/s/Exec=/Exec=env GTK_THEME=Arc-Lighter /g" && \
    sed -i "/^Categories=/s/^.*$/Categories=Office;FlowChart;/" ~/.local/share/applications/libreoffice-draw.desktop && \
    echo "LibreOffice is installed."
else
    echo "LibreOffice is not installed."
fi

if which meld | grep -q meld; then
    gsettings set org.gnome.meld custom-font 'hack 9' && \
    gsettings set org.gnome.meld show-line-numbers true && \
    gsettings set org.gnome.meld style-scheme 'solarized-light' && \
    gsettings set org.gnome.meld use-system-font false && \
    echo "Meld is installed."
else
    echo "Meld is not installed."
fi

if which mc | grep -q mc; then
    echo "Midnight Commander is installed."
else
    rm -rf ~/.config/mc && \
    rm -f ~/.local/share/applications/mc.desktop && \
    rm -f ~/.local/share/applications/mcedit.desktop && \
    echo "Midnight Commander is not installed."
fi

if which mpv | grep -q mpv; then
    echo "mpv is installed."
else
    rm -rf ~/.config/mpv && \
    rm -f ~/.local/share/applications/mpv.desktop && \
    echo "mpv is not installed."
fi

if which qt5ct | grep -q qt5ct; then
    sed -i "/qt5ct/s/^# //" ~/.xsessionrc && \
    echo "Qt5 Configuration Tool is installed."
else
    rm -rf ~/.config/qt5ct && \
    echo "Qt5 Configuration Tool is not installed."
fi

if which qv4l2 | grep -q qv4l2; then
    echo "Qt V4L2 test utility is installed."
else
    rm -f ~/.local/share/applications/qv4l2.desktop && \
    echo "Qt V4L2 test utility is not installed."
fi

if which qvidcap | grep -q qvidcap; then
    echo "Qt V4L2 video capture utility is installed."
else
    rm -f ~/.local/share/applications/qvidcap.desktop && \
    echo "Qt V4L2 video capture utility is not installed."
fi

if which redshift | grep -q redshift; then
    echo "Redshift is installed."
else
    rm -f ~/.config/autostart/geoclue-demo-agent.desktop && \
    rm -f ~/.config/autostart/redshift.desktop && \
    rm -f ~/.config/redshift.conf && \
    echo "Redshift is not installed."
fi

if which xterm | grep -q xterm; then
    echo "xterm is installed."
else
    rm -f ~/.local/share/applications/debian-uxterm.desktop && \
    rm -f ~/.local/share/applications/debian-xterm.desktop && \
    echo "xterm is not installed."
fi
