#!/bin/bash

if uname -a | grep -iq debian; then
    mycursorlight=Breeze_Snow
    mycursordark=breeze_cursors
else
    mycursorlight=Breeze_Light
    mycursordark=Breeze
fi

if xfconf-query -c xsettings -p /Net/ThemeName | grep -q Arc-Dark; then
    xfconf-query -c xfce4-panel -p /panels/dark-mode -s false
    xfconf-query -c xfwm4 -p /general/theme -s Arc-Lighter
    xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$mycursordark"
    xfconf-query -c xsettings -p /Net/IconThemeName -s Papirus-Light
    xfconf-query -c xsettings -p /Net/ThemeName -s Arc-Lighter
    gsettings set org.xfce.mousepad.preferences.view color-scheme 'kate'
else
    xfconf-query -c xfce4-panel -p /panels/dark-mode -s true
    xfconf-query -c xfwm4 -p /general/theme -s Arc-Dark
    xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$mycursorlight"
    xfconf-query -c xsettings -p /Net/IconThemeName -s Papirus-Dark
    xfconf-query -c xsettings -p /Net/ThemeName -s Arc-Dark
    gsettings set org.xfce.mousepad.preferences.view color-scheme 'oblivion'
fi

xfce4-panel -r && xfwm4 --replace
