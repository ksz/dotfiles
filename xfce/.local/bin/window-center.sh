#!/bin/bash

function_center () {
    IFS='x' read screenWidth screenHeight < <(xdpyinfo | grep dimensions | grep -o '[0-9x]*' | head -n1)
    width=$(xdotool getactivewindow getwindowgeometry --shell | head -4 | tail -1 | sed 's/[^0-9]*//')
    height=$(xdotool getactivewindow getwindowgeometry --shell | head -5 | tail -1 | sed 's/[^0-9]*//')
    newPosX=$((screenWidth/2-width/2))
    newPosY=$((screenHeight/2-height/2))
    xdotool getactivewindow windowmove "$newPosX" "$newPosY"
}

function_display1 () {
    list=$(wmctrl -l | grep -v -E " Desktop$| xfce4-panel$| conky \(" | cut -d " " -f1)
    for window in $list; do
        xdotool windowactivate --sync $window
        function_center
        xdotool windowminimize --sync $window
    done
}

function_display2 () {
    list=$(wmctrl -l | grep -v -E " Desktop$| xfce4-panel$| conky \(" | cut -d " " -f1)
    for window in $list; do
        xdotool windowactivate --sync $window
        xdotool getactivewindow windowmove 500 1000
        xdotool windowminimize --sync $window
    done
}

function_display3 () {
    list=$(wmctrl -l | grep -v -E " Desktop$| xfce4-panel$| conky \(" | cut -d " " -f1)
    for window in $list; do
        xdotool windowactivate --sync $window
        xdotool getactivewindow windowmove 2500 1000
        xdotool windowminimize --sync $window
    done
}

case "$1" in
    -c) function_center ;;
    -1) function_display1 ;;   # HDMI-1 || DP-1
    -2) function_display2 ;;   # DP-1 & DP-2
    -3) function_display3 ;;   # HDMI-1 & DP-1 & DP-2
esac
